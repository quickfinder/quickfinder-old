
$(document).ready(function() {

	$('.citysearch').flexdatalist({
		 minLength: 1,
		 searchContain:true,/*true allows matches starting from anywhere within a word*/
		 maxShownResults :20,

	});

	$('.productsearch').flexdatalist({
		 valueProperty: '*',
         selectionRequired: true,
		 maxShownResults :20
	});

	$('.areasearch').flexdatalist({
		 minLength: 1,
		 searchContain:true,/*true allows matches starting from anywhere within a word*/
		 maxShownResults :20
	});
	
	
});