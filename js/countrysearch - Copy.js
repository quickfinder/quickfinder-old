
$(document).ready(function() {

	$('.citysearch').flexdatalist({
		 minLength: 1,
		 searchContain:true,/*true allows matches starting from anywhere within a word*/
		 maxShownResults :20,

	});

	$('.productsearch').flexdatalist({
		 minLength: 1,
		 searchContain:true,/*true allows matches starting from anywhere within a word*/
		 maxShownResults :20
	});

	$('.areasearch').flexdatalist({
		 minLength: 1,
		 searchContain:true,/*true allows matches starting from anywhere within a word*/
		 maxShownResults :20
	});
	
	
	/***set area search box visible **/
	$('input.flexdatalist').on('before:flexdatalist.search', function(event, keywords, data) 
	{
		alert("Before searching");
		
		var productsearch='.productsearch';/**classname**/
		//last() for newely appended productsearch textbox by jquery plugin
		var height=$(productsearch).last().outerHeight(); 
		var width=$(productsearch).last().outerWidth(); 
		
		//var Top = $(productsearch).last().position().top;
		//var Left = $(productsearch).last().position().left;
		
		//alert("height :" + height);
		//alert("width :" +width);
		//alert("Top :" + Top);
		//alert("Left :" + Left);
		
		/***set width of area search textbox ***/
		$('.areasearch').removeClass('hidden');
		$('.areasearch').last().outerWidth(width);
		
	});
	

});