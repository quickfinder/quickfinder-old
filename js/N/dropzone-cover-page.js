
Dropzone.autoDiscover = false;

$(document).ready(function(){
	
/**global vars***/
filenames=[];
file_index = 0;

// Dropzone class:
var myDropzone = new Dropzone("div#mydropzone", {
    url: "upload/img",
    acceptedFiles: "image/*",
    //thumbnailWidth: 250,  //and modify .dropzone .dz-preview .dz-image class values in css
    //thumbnailHeight: 250,
    addRemoveLinks: true,
    //maxFiles: 1,
    /***remove files if limit crossed***/
    init: function() {
        this.on("maxfilesexceeded", function(file) {
            this.removeAllFiles();
            this.addFile(file);
        });

        this.on("removedfile", function(file) {
            var filename = file.previewElement.id;
			//alert(filename);
			$.ajax({
				url: "delete.php",
				type: "POST",
				data: { 'filename': filename}
			}).done(function (result) {
				//alert(result);
				var obj = JSON.parse(result);
				
				alert(" obj.success : "+obj.success);
				alert(" obj.msg : "+obj.msg);
				
				/*** remove file element form filenames array*/
				var removeItem = filename;
				filenames = _.without(filenames, removeItem); //underscore.js function
				
            });
		
        });
    },
    error: function(file, message, xhr) {
        //$(file.previewElement).remove();
        //alert("Change file name and upload");
    },
    success: function(file, response) {
        alert(response);

        response = JSON.parse(response);

		/*** store to get list of all uploaded files**/
        filenames[file_index] = response.filename;
		file_index = file_index+1;

		/*** save id in preview window to get id back when deleting*/
		file.previewElement.id = response.filename;
		if (file.previewElement) {
		  return file.previewElement.classList.add("dz-success");
		}
    }
});

});


//"audio/*,image/*,.psd,.pdf"

