<?php

class Banner_model extends CI_Model {

    public function getBannerData() {
       return $this->db->get('banner')->result_array();
    }
	public function getDetails($id)
	{
		return $this->db->where('id',$id)->get('banner')->row(); 
	}
    public function insertBanner($filename) {
        $data = array(
            'images' => $filename,
			'startdate'=>$_POST['startdate'],
			'enddate'=>$_POST['enddate'],
        );
        return $this->db->insert('banner', $data);
    }
	public function editBanner($filename,$id)
	{
		$data = array(
            'images' => $filename,
			'startdate'=>$_POST['startdate'],
			'enddate'=>$_POST['enddate'],
        );
        return $this->db->where('id',$id)->update('banner', $data);
	}
	public function activeBanner($id)
	{
		$data=array('IsActive'=>1);
		return $this->db->where('id',$id)->update('banner',$data);
	}
	public function deactiveBanner($id)
	{
		$data=array('IsActive'=>0);
		return $this->db->where('id',$id)->update('banner',$data);
	}
	public function bannerdelete($id)
	{
		return $this->db->where('id',$id)->delete('banner');
	}
	public function addPlatinumPlan($month,$year,$store_id)
	{
		$count=$this->db->select('storeid,calender.count as bcount')->where('monthID',$month)->where('year',$year)->get('calender')->row();
		$bcount=$count->bcount;
		$prevstoreid=$count->storeid;
		if(!empty($prevstoreid))
			$prevstoreid.=','.$store_id;
		else
			$prevstoreid=$store_id;
		
		$data=array(
					'storeid'=>$prevstoreid,
					'count'=>$bcount+1,
		);
		return $this->db->where('monthID',$month)->where('year',$year)->update('calender',$data);
	}
}

?>
