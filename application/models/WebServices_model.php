<?php
class WebServices_model extends CI_Model {

	public function getStores()
	{
		$this->db->select('stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->get()->result_array();	
		
	}
     
    
	public function userCount()
	{
		$row=$this->db->where('date',date('Y-m-d'))->get('sitevisit')->num_rows();
		if($row > 0)
		{
			$count=$this->db->where('date',date('Y-m-d'))->get('sitevisit')->row();
			$data=array('visits'=>$count->visits+1);
			return $this->db->where('date',date('Y-m-d'))->update('sitevisit',$data);
		}
		else{
			$data=array('visits'=>1,'date'=>date('Y-m-d'));
			return $this->db->insert('sitevisit',$data);
		}
	}



public function changePwdUser()
	{
		// print_r($_REQUEST);
		$userid=$_REQUEST['userid'];
		// die;
		$this->db->where('id',$userid);
        if ($this->db->get('user')->num_rows() == 1) {

		$data=array('password'=>md5($_REQUEST['cpwd']));
		return $this->db->where('id',$userid)->update('user',$data);

     } else {

		return false;
        }
		
	}


	public function getSearch()
	{	
						// print_r($_REQUEST);
// die;
		$search=trim($_REQUEST['search']);
		$city=$_REQUEST['city'];
		if($_REQUEST['area']=='All')
		{
			$area='';
		}
		else
		{
			$area=$_REQUEST['area'];
		}
		$searchlen= strlen($search);
		$query=explode(" ",$search);
		// echo $query;
		// die;
		$records=array();
			if(!empty($city) && !empty($search) && !empty($area))
			{
				return $this->ifNotNullSearch($city,$query,$area);
			}
			elseif(!empty($city) && !empty($search) && empty($area))
			{			
				return $this->ifAreaNull($city,$query);
			}
			elseif(!empty($city) && empty($search) && !empty($area))
			{			
				return $this->ifSearchNull($city,$area);
			}
			elseif(!empty($city) && empty($search) && empty($area))
			{			
				return $this->ifSearchAreaNull($city);
			}
			elseif(empty($city) && !empty($search) && empty($area))
			{			
				return $this->ifCityAreaNull($query);
			}else{
				// echo "no data";
				
			}
			
	}
	public function ifSearchNull($city,$area)
	{

		$records=array();
		$sql="SELECT  stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu' and `stores`.`area` = '$area'  ";
		$premiumRecords=$this->db->query($sql)->result_array();
		foreach($premiumRecords as $row)
			{
				$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id'])
					);
			}				
		return $records;
	}
	public function ifCityAreaNull($query)
	{
		// print_r($query);
		// die;
		$records=array();
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				// echo $condition;
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,store_payment.IsActive FROM `categories` JOIN `stores` JOIN `store_payment` WHERE `store_payment`.`IsActive` = 1 AND ($condition)";
				// echo  $sql;
				$premiumRecords=$this->db->query($sql)->result_array();
				foreach($premiumRecords as $row)
				{
					// print_r($row);
					
					$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id'])
					);
				}
				return $records;
	}
	public function ifAreaNull($city,$query)
	{
		$condition='';
		$records=array();
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu'  and  ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id'])
					);
				}				
				return $records;
	}
	public function ifSearchAreaNull($city)
	{
		$records=array();
		$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu' ";
		$premiumRecords=$this->db->query($sql)->result_array();
		foreach($premiumRecords as $row)
			{
				
				$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id'])
					);
			}				
		return $records;
	}
	public function ifNotNullSearch($city,$query,$area)
	{
		$records=array();
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu' and `stores`.`area`='$area' and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				foreach($premiumRecords as $row)
				{
					
					$records[]=array(
					'row'=>$row,
					'rating'=>$this->getRating($row['id'])
					);
				}
				return $records;
	}	
	public function get_mysqli()
	{
		$db = (array)get_instance()->db;
		return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
	}
	public function getRating($storeid)
	{
		$this->db->distinct();
		$this->db->select('sum(rating) as rating,count(*) as row');
		$data=$this->db->where('storeid',$storeid)->get('store_rating')->result_array(); 
		if($data[0]['row']==0)
			return 0;
		else
			return  ceil($data[0]['rating']/$data[0]['row']);
	}


	function get_names_for_autofill()
	{
        $query = $this->db->query("SELECT `name` FROM `categories`
                                   UNION
                                   SELECT `name` FROM `subcategories`
                                   UNION
                                   SELECT `name` FROM stores s, store_payment sp
                                          WHERE sp.storeid= s.id and sp.IsActive=1"
		                         );
        return $query->result_array();
    }

	function get_all_areas()
	{
        return  $this->db->select('name')
			              ->from('area')
			              ->get()
			              ->result_array();
	}


    public function get_banners()
    {
        return  $this->db->select('*')
			             ->from('homebanners')
                         ->get()
                         ->result_array();
    }

     public function getCategories()
    {
        return  $this->db->select('*')
			             ->from('categories')
                         ->get()
                         ->result_array();
    }  

         public function getSubcategories($id)
    {
    	// print_r($id);
        return  $this->db->select('*')
			             ->from('subcategories')
			             ->where('category_id',$id)
                         ->get()
                         ->result_array();

    } 
     public function getStoreRatingOverall($id) {
     	// print_r($id);
        $records=array();
        $poor =0;
        $average =0;
        $good =0;
        $verygood =0;
        $excellence =0;
        $ratevalue =0;
        $this->db->select('*');
        $this->db->from('store_rating');
        $result_array = $this->db->where('storeid', $id)->get()->result_array();
        // print_r($result_array);
        foreach ($result_array as $row) {
            $ratevalue = $ratevalue + (int)$row['rating'];
            if ($row['rating'] == 1) {
               $poor++;
            } elseif ($row['rating'] == 2) {
               $average++;
            } elseif ($row['rating'] == 3) {
                $good++;
            } elseif ($row['rating'] == 4) {
               $verygood++;
            } elseif ($row['rating'] == 5) {
                $excellence++;
            }

        $records[] = array(
            'rating' => $row['rating'],
            'value' => $ratevalue,
            'review' => $row['review'],
            'userid' => $row['userid'],
            'poor' => $poor,
            'average' => $average,
            'good' => $good,
            'verygood' => $verygood,
            'excellence' => $excellence,
            'user_details' => $this->userDetails($row['userid']),
        );
        }
        // print_r($records);
        return $records;
    }

    //add registartion form
public function userRegister($fname,$lname,$email,$mobile,$username,$password){
        $data = array(
            'usertype' => 1,
			'isactive' => 0,
            'email' => $email,
			'mobile'=>$mobile,
			'username'=>$username,
            'password' => md5($password),
        );
        $this->db->insert('user', $data);
        $id = $this->db->insert_id();
        $data = array(
            'first_name'=>$fname,
			'last_name'=>$lname,
			'mobile'=>$mobile,
			'email' => $email,
            'userid' => $id,
        );
        $this->db->insert('userdetails', $data);
        return $id;
    }


    public function checkEmail($email) {
    	// print_r($email);
    	// die;
        $this->db->where('email',$email);
        if ($this->db->get('user')->num_rows() == 1) {
            return false;
        } else {
            return true;
        }
    }

public function checkUniqueUserName($username) {
    	// print_r($email);
    	// die;
        $this->db->where('username',$username);
        if ($this->db->get('user')->num_rows() == 1) {
            return false;
        } else {
            return true;
        }
    }

  public function checkUserEmailId($email) {
        $this->db->where('email',$email);
        if ($this->db->get('user')->num_rows() == 1) 
		{
			return $this->db->where('email',$email)->get('user')->row();
        } 
		else 
		{
            return FALSE;
        }
    }
 public function checkUniqueEmail($email) {
    	// print_r($email);
    	// die;
        $this->db->where('email',$email);
        if ($this->db->get('user')->num_rows() < 1) {
            return true;
        } else {
            return false;
        }
    }
     public function userLoginDetails($username,$password) {
       // echo $username;
       // die;
       // $this->db->select('username,password,email');
        //$this->db->from('userdetails');
         //$this->db->where('username', $username );
     
		$this->db->where('email', $username);
        $this->db->or_where('username', $username);
        $this->db->where('password', md5($password) );
        
         if ($this->db->get('user')->num_rows() == 1) {

            $data=array('isnew'=>'1');
			$this->db->where('username', $username);
			$this->db->or_where('email',$username);
			$this->db->where('password',md5($password));
			$this->db->update('user',$data);
			
			$this->db->select('*');
			$this->db->from('user');
			$this->db->join('userdetails','user.id=userdetails.userid');
			$this->db->where('user.username', $username);
            $this->db->or_where('user.email', $username);
            $this->db->where('user.password', md5($password));
            $this->db->where('user.isactive',1);
            return $this->db->get()->result_array();

            
        } else {
            return false;
        }
    }


    public function userDetails($param) {
//        echo $param;
        $this->db->select('first_name,last_name,email');
        $this->db->from('userdetails');
        return $this->db->where('userid', $param)->get()->result_array();
    }


     public function userDetailsInfo($storeid) {
       // echo $storeid;
       // die;
        $this->db->select('store_rating.storeid,store_rating.userid,userdetails.userid,userdetails.first_name,userdetails.last_name,userdetails.email,userdetails.mobile,userdetails.profilepic');
        $this->db->from('store_rating');
        $this->db->join('userdetails', 'userdetails.userid=store_rating.userid');
        // $this->db->join('store_payment', 'store_payment.storeid=stores.id');
        // $this->db->join('payment', 'payment.id=store_payment.paymentid');
        return $premiumRecords = $this->db->where('store_rating.storeid', $storeid)->get()->result_array();
    }

      public function getStore($id,$subid)
    {
    	// print_r($id);
        // return  $this->db->select('*')
			     //         ->from('stores')
			     //         ->where('categoryid',$id)
			     //         ->where('subcategoryid',$subid)
        //                  ->get()
        //                  ->result_array();

                         $records=array();
        $this->db->select('stores.user_clicks,stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
        $this->db->from('stores');
        $this->db->join('categories', 'categories.id=stores.categoryid');
        $this->db->join('store_payment', 'store_payment.storeid=stores.id');
        $this->db->join('payment', 'payment.id=store_payment.paymentid');
        $premiumRecords = $this->db->where('subcategoryid', $subid , 'categoryid', $id)->get()->result_array();
        foreach ($premiumRecords as $row) {
            $records[] = array(
                'row' => $row,
                'rating' => $this->getRating($row['id'])
            );
        }
            	// print_r($records);

        return $records;

    }   
}
?>
	