<?php
class Validation_model extends CI_Model
{
	public function check_email($str)
	{
		if($this->session->userdata('newStoreUserID'))
		{
			if($this->session->userdata('firstpage')==1)
			{
				$data=$this->db->select('email')->where('id',$this->session->userdata('newStoreUserID'))->get('user')->row();
				$email=$data->email;
				if(strcmp($str,$email))
				{		
					$this->db->where('email',$str);
					$this->db->or_where('email',$email);
					$count=$this->db->get('user')->num_rows();
					
					if($count==2)
					{
						$this->session->unset_userdata('checkemail');
						$this->session->unset_userdata('prevEmail');
						return false;
					}
					elseif($count==1)
					{
						$this->session->unset_userdata('checkemail');
						$this->session->unset_userdata('prevEmail');
						return true;
					}
				}
				else
				{
					return true;
				}
				
			}
			else 
				{
				$email=$_POST['prevemail'];
				if(strcmp($str,$email))
				{		
					$this->db->where('email',$str);
					$this->db->or_where('email',$email);
					$count=$this->db->get('user')->num_rows();
					
					if($count==2)
					{
						$this->session->unset_userdata('checkemail');
						$this->session->unset_userdata('prevEmail');
						return false;
					}
					elseif($count==1)
					{
						$this->session->unset_userdata('checkemail');
						$this->session->unset_userdata('prevEmail');
						return true;
					}
				}
				else
				{
					return true;
				}
				}
	
		}
		else
		{
			
			$this->db->where('email',$str);
			if($this->db->get('user')->num_rows()==1)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	public function username_check($str)
	{
		if($this->session->userdata('newStoreUserID'))
		{
			if($this->session->userdata('firstpage')==1)
			{
				$data=$this->db->select('username')->where('id',$this->session->userdata('newStoreUserID'))->get('user')->row();
				$username=$data->username;
				
				
				if(strcmp($str,$username))
				{		
				$this->db->where('username',$str);
				$this->db->or_where('username',$username);
				$count=$this->db->get('user')->num_rows();
				
				if($count==2)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return false;
				}
				elseif($count==1)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return true;
				}
				}
				else
				{
					return true;
				}
				
			}
			else
				{
					$username=$_POST['prevusername'];
				$str=$_POST['username'];
				if(strcmp($str,$username))
				{		
				$this->db->where('username',$str);
				$this->db->or_where('username',$username);
				$count=$this->db->get('user')->num_rows();
				
				if($count==2)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return false;
				}
				elseif($count==1)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return true;
				}
			}
			else
			{
				return true;
			}
				}
		}
		else
		{
			$this->db->where('username',$str);
			if($this->db->get('user')->num_rows()==1)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	public function editCheck_email($str)
	{
			$email=$this->session->userdata('prevEmail');
			if(strcmp($str,$email))
			{		
				$this->db->where('email',$str);
				$this->db->or_where('email',$email);
				$count=$this->db->get('user')->num_rows();
				
				if($count==2)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return false;
				}
				elseif($count==1)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return true;
				}
			}
			else
			{
				return true;
			}
	}
	public function check_username_profile($str)
	{
		
		$email=$this->session->userdata('prevUsername');
			if(strcmp($str,$email))
			{		
				$this->db->where('username',$str);
				$this->db->or_where('username',$email);
				$count=$this->db->get('user')->num_rows();
				
				if($count==2)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return false;
				}
				elseif($count==1)
				{
					$this->session->unset_userdata('checkemail');
					$this->session->unset_userdata('prevEmail');
					return true;
				}
			}
			else
			{
				return true;
			}
	}
	public function check_emailForgot($str)
	{
		
		$this->db->where('email',$str);
		if($this->db->get('user')->num_rows()==1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function password_check($str)
	{
		$this->db->where('password',md5($str));
		$this->db->where('id',$this->session->userdata('userid'));
		if($this->db->get('user')->num_rows()==1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>