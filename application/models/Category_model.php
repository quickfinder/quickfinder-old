<?php
class Category_model extends CI_Model
{
	
	public function addNewCategory($filename)
	{
		$string = str_replace(' ', '', $_POST['name']);
		$date = date_create();
		$data=array(
		'name'=>$this->usertype=$_POST['name'],
		'icon_name'=>$string,
		'icons'=>$filename,
		'createdat'=> date_timestamp_get($date),
		'updatedat'=> date_timestamp_get($date),
		);
		return $this->db->insert('categories',$data);
	}
	public function deactiveCategory($id)
	{
		$data=array('IsActive'=>0);
		return $this->db->where('id',$id)->update('categories',$data);
	}
	public function activeCategory($id)
	{
		$data=array('IsActive'=>1);
		return $this->db->where('id',$id)->update('categories',$data);
		
	}
	public function getSubCategory($id)
	{
			   $this->db->select('categories.name as categoriesname,categories.id as categoriesid,subcategories.name as subcategoriesname');
			   $this->db->from('categories');
			   $this->db->join('subcategories','categories.id=subcategories.category_id');
		$data= $this->db->where('categories.id',$id)->get()->result_array();
		if(!empty($data))
		{
			return $data;
		}
		else{
			
				
			   $row=$this->db->where('id',$id)->get('categories')->result();
			   $data[0]= array('categoriesname'=>$row[0]->name,
							'categoriesid'=>$id,
							'subcategoriesname'=>''
							);
							return $data;
		}
	}
	public function addSubCategory()
	{
		$id=$_POST['id'];
		$subcategory=$_POST['subcategory'];
		$a=explode(',',$subcategory);
		for($i=0;count($a)>$i;$i++)
		{
			$data=array(
			'category_id'=>$id,
			'name'=>$a[$i],
			);
			$this->db->insert('subcategories',$data);
		}
		return true;
	}
	public function getCategory()
	{
		$categories = $this->db->get('categories')->result_array();
		
		$final_array = array();
		
		foreach($categories as $category)
		{
			$category_id = $category['id'];
			
			$query = $this->db->select('*')
			     ->from('subcategories')
		  	     ->where('category_id', $category_id)
			     ->get();
				 
			//print_r($query->result_array());
			//die();
			
			array_push($final_array,array(
			  'category'=>$category,
			  'subcategories'=>$query->result_array()
			));
		}
		
		return $final_array;
		
	}
	public function categorydelete()
	{
		return $this->db->where('id',$_POST['id'])->delete('categories');
	}
	public function changeSubcategory()
	{
		 $cid=$_POST['id'];
		 $tag=$_POST['tag'];
		 $val=$_POST['val'];
		 $data=array('name'=>$val);
		 return $this->db->where('category_id',$cid)->where('name',$tag)->update('subcategories',$data);
	}
	public function deleteSubcategory()
	{
		 $cid=$_POST['id'];
		 $val=$_POST['val'];
		 return $this->db->where('category_id',$cid)->where('name',$val)->delete('subcategories');
	}
	public function addNewSubcategory()
	{
		$data=$this->db->where('category_id',$_POST['id'])->where('name',strtolower($_POST['val']))->get('subcategories')->num_rows();
		if($data == 0)
		{
			$data=array(
			'category_id'=>$_POST['id'],
			'name'=>strtolower($_POST['val']),
			);
			return $this->db->insert('subcategories',$data);
		}
		else
		{
			return false;
		}
	}
	public function getCategoryDetails($id)
	{
		return $this->db->where('id',$id)->get('categories')->result();
	}
	public function editCategories($filename,$id)
	{
		$string = str_replace(' ', '', $_POST['name']);
		$date = date_create();
		$data=array(
		'name'=>$this->usertype=$_POST['name'],
		'icon_name'=>$string,
		'icons'=>$filename,
		'createdat'=> date_timestamp_get($date),
		'updatedat'=> date_timestamp_get($date),
		);
		return $this->db->where('id',$id)->update('categories',$data);
	}
	
	
}
?>