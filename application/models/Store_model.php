<?php

class Store_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $id = $this->session->userdata('userid');
    }
	public function getSubCategory($id)
	{
		return $this->db->where('category_id',$id)->get('subcategories')->result_array();
	}

	public function getStoreCoverImg($store_id)
    {
        return $this->db->select('firstimage')
                        ->from('stores')
                        ->where('id',$store_id)
                        ->get()
                        ->result_array();
    }

    public function get_store_multi_images($store_id)
    {
        return $this->db->select('images')
            ->from('stroe_images')
            ->where('storeid',$store_id)
            ->get()
            ->result_array();
    }

    public function updateStoreData()
    {
        $store_id = $this->session->userdata('storeid');
        $img_folder = 'storeimages/';
			if($_POST['subcategoryid']=='other')
				{
					$row=$this->db->where('id',$this->session->userdata('storeid'))->get('othercategory')->num_rows();
					if($row == 1)
					{
						$othercat=array('name'=>$_POST['other'],'creatdat'=>date('Y-m-d'),'updatedat'=>date('Y-m-d'));
						$this->db->where('storeid',$this->session->userdata('storeid'))->update('othercategory',$othercat);
					}
					else
					{
						$othercat=array('storeid'=>$this->session->userdata('storeid'),'name'=>$_POST['other'],'creatdat'=>date('Y-m-d'),'updatedat'=>date('Y-m-d'));
						$this->db->insert('othercategory',$othercat);
					}
				}
				else{
					$this->db->where('storeid',$this->session->userdata('storeid'))->delete('othercategory');
				}
        $newdata = array(
            'categoryid' => $this->input->post('categoryid'),
            'subcategoryid' => $this->input->post('subcategoryid'),
            'description' => $this->input->post('description'),
            'firstimage'=> $img_folder.$this->input->post('img'),
            'email' => $this->input->post('email'),
            'mobile' => $this->input->post('mobile'),
            'area' => $this->input->post('area'),
            'city' => $this->input->post('city'),
            'address' => $this->input->post('address'),
            'state' => $this->input->post('state'),
            'country' => $this->input->post('country'),
            'updatedat' => date('Y-m-d H:m:s')
        );

        $update = $this->db->where('id',$store_id)
                           ->update('stores',$newdata);

        if($update)
        {
            $store_multi_imgs = json_decode($this->input->post('store_multi_imgs'));
            //print_r($store_multi_imgs);die();

            /*remove previous store images*/
            $delete = $this->db->where('storeid', $store_id)
                               ->delete('stroe_images');

            if($delete)
            {
                if($store_multi_imgs != NULL)
                {
                    foreach($store_multi_imgs as $img)
                    {
                        /* insert new images*/
                        $data = array(
                            'storeid' => $store_id,
                            'images' => $img_folder . $img,
                            'updatedat' => date('Y-m-d H:m:s')
                        );

                        $this->db->insert('stroe_images', $data);
                    }
                    //echo "Updated both tables";
                    return true;
                }
            }
        }
        return false;
    }

    public function addNewStore($filename, $uploadData) {
    	/*$tags =$_POST['tags'];
       $addtag='';
       for ($i=0;count($tags)>$i;$i++) {
       $addtag .= " ".$tags[$i];
       }*/
	  
		//create store code
        $date = date_create();
        $data = array(
            'userid' => $this->session->userdata('userid'),
            'categoryid' => $this->categoryid = $_POST['categoryid'],
            'subcategoryid' => $this->subcategoryid = $_POST['subcategoryid'],
            'name' => $this->name = $_POST['name'],
            'email' => $this->email = $_POST['email'],
            'mobile' => $this->mobile = $_POST['mobile'],
			'country'=>$this->country = $_POST['country'],
			'state'=>$this->country = $_POST['state'],
			'city'=>$this->country = $_POST['city'],
			'area'=>$this->country = $_POST['area'],
            'description' => $this->description = $_POST['description'],
            'address' => $this->description = $_POST['address'],
			//'tags'=>$addtag,
            'firstimage' => $this->firstimage = $filename,
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
        );
        $this->db->insert('stores', $data);
        $id = $this->db->insert_id();
		if($this->session->userdata('plan')==1)
				$this->entryPlan(1,$id);
		else
				$this->entryPlanWithoutPay(1,$id);	
        
		//multiple file upload code here
        if ($id) {
            if ($uploadData != '') {
                for ($j = 0; $j < count($uploadData); $j++) {
                    $data1 = array(
                        'images' => $this->images = $uploadData[$j]['file_name'],
                        'storeid' => $this->storeid = $id,
                        'createdat' => date('Y-m-d H:m:s'),
                        'updatedat' => date('Y-m-d H:m:s'),
                    );
                    $this->db->insert('stroe_images', $data1);
                }
                return TRUE;
            } else {
                return TRUE;
            }
        }
    }
	public function adminaddNewStore($filename, $uploadData) {
    	/*$tags =$_POST['tags'];
       $addtag='';
       for ($i=0;count($tags)>$i;$i++) {
       $addtag .= " ".$tags[$i];
       }*/
		//create store code
        $date = date_create();
        $data = array(
            'userid' => $this->session->userdata('vendorID'),
            'categoryid' => $this->categoryid = $_POST['categoryid'],
            'subcategoryid' => $this->subcategoryid = $_POST['subcategoryid'],
            'name' => $this->name = $_POST['name'],
            'email' => $this->email = $_POST['email'],
            'mobile' => $this->mobile = $_POST['mobile'],
			'country'=>$this->country = $_POST['country'],
			'state'=>$this->country = $_POST['state'],
			'city'=>$this->country = $_POST['city'],
			'area'=>$this->country = $_POST['area'],
            'description' => $this->description = $_POST['description'],
            'address' => $this->description = $_POST['address'],
			//'tags'=>$addtag,
            'firstimage' => $this->firstimage = $filename,
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
			'adminid'=>$this->session->userdata('userid'),
        );
		$this->session->unset_userdata('vendorID');
        $this->db->insert('stores', $data);
        $id = $this->db->insert_id();
		if($this->session->userdata('plan')==1)
				$this->entryPlan(1,$id);
		else
				$this->entryPlanWithoutPay(1,$id);	
        
		//multiple file upload code here
        if ($id) {
            if ($uploadData != '') {
                for ($j = 0; $j < count($uploadData); $j++) {
                    $data1 = array(
                        'images' => $this->images = $uploadData[$j]['file_name'],
                        'storeid' => $this->storeid = $id,
                        'createdat' => date('Y-m-d H:m:s'),
                        'updatedat' => date('Y-m-d H:m:s'),
                    );
                    $this->db->insert('stroe_images', $data1);
                }
                return TRUE;
            } else {
                return TRUE;
            }
        }
    }
	//admin add new stores for vendor without vender id
	public function admin_store($filename, $uploadData) {
		
    	/*$tags =$_POST['tags'];
       $addtag='';
       for ($i=0;count($tags)>$i;$i++) {
       $addtag .= " ".$tags[$i];
       }*/
		//create store code
		if (!$this->session->userdata('newStoreID')) 
		{
		$data=array(
		'usertype'=>2,
		'mobile'=>$this->mobile=$_POST['usermobile'],
		'email'=>$this->email=$_POST['useremail'],
		'password'=>$this->password=md5($_POST['password']),
		);
		$this->db->insert('user',$data);
		 $vendorID=$this->db->insert_id();
		
		$data=array(
		'first_name'=>$_POST['fname'],
		'last_name'=>$_POST['lname'],
		'mobile'=>$this->mobile=$_POST['usermobile'],
		'email'=>$this->email=$_POST['useremail'],
		'userid'=>$vendorID,
		);
		$this->db->insert('userdetails',$data);
        $date = date_create();
        $data = array(
            'userid' => $vendorID,
            'categoryid' => $this->categoryid = $_POST['categoryid'],
            'subcategoryid' => $this->subcategoryid = $_POST['subcategoryid'],
            'name' => $this->name = $_POST['name'],
            'email' => $this->email = $_POST['email'],
            'mobile' => $this->mobile = $_POST['mobile'],
			'country'=>$this->country = $_POST['country'],
			'state'=>$this->country = $_POST['state'],
			'city'=>$this->country = $_POST['city'],
			'area'=>$this->country = $_POST['area'],
            'description' => $this->description = $_POST['description'],
            'address' => $this->description = $_POST['address'],
			//'tags'=>$addtag,
            'firstimage' => $this->firstimage = $filename,
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
			'adminid'=>$this->session->userdata('userid'),
        );
        $this->db->insert('stores', $data);
        $id = $this->db->insert_id();
		if($this->session->userdata('plan')==1)
				$this->entryPlan(1,$id);
		else
				$this->entryPlanWithoutPay(1,$id);	
		//multiple file upload code here
        if ($id) {
            if ($uploadData != '') {
                for ($j = 0; $j < count($uploadData); $j++) {
                    $data1 = array(
                        'images' => $this->images = $uploadData[$j]['file_name'],
                        'storeid' => $this->storeid = $id,
                        'createdat' => date('Y-m-d H:m:s'),
                        'updatedat' => date('Y-m-d H:m:s'),
                    );
                    $this->db->insert('stroe_images', $data1);
                }
                return TRUE;
            } else {
                return TRUE;
            }
        }
		}
		else
		{
			$date = date_create();
			$data = array(
            'userid' => $this->session->userdata('newStoreUserID'),
            'firstimage' => $this->firstimage = $filename,
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
			'adminid'=>$this->session->userdata('userid'),
			);
        $this->db->where('id', $this->session->userdata('newStoreID'));
         $this->db->update('stores', $data);
		 $id=$this->session->userdata('newStoreID');
			$this->session->unset_userdata('newStoreID');
			$this->session->unset_userdata('newStoreUserID');
			$this->session->unset_userdata('firstpage');
			
		if($this->session->userdata('plan')==1)
				$this->entryPlan(1,$id);
		else
				$this->entryPlanWithoutPay(1,$id);	
		//multiple file upload code here
        if ($id) {
            if ($uploadData != '') {
                for ($j = 0; $j < count($uploadData); $j++) {
                    $data1 = array(
                        'images' => $this->images = $uploadData[$j]['file_name'],
                        'storeid' => $this->storeid = $id,
                        'createdat' => date('Y-m-d H:m:s'),
                        'updatedat' => date('Y-m-d H:m:s'),
                    );
                    $this->db->insert('stroe_images', $data1);
                }
					$data1 ['email'] = $_POST['useremail'];
                    $data1 ['username'] =$_POST['username'];
                    $data1 ['password'] = $_POST['username'].'@123';

                    $this->load->library('email');
                    $config = array(
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'wordwrap' => TRUE,
                        'priority' => '1'
                    );
                    $this->email->initialize($config);
                    
                    $this->email->from('abhishek@trigensoft.com', 'Quickfinder');
                    $this->email->to($data1['email']);
                    $this->email->subject('Activate your store ' .$data1['username'].',');
                    $body = $this->load->view('emailtemplate/storeadd', $data1, TRUE);
                    $this->email->message($body);

                    if ($this->email->send())
					{

                         return TRUE;
                    }
               
            } else {
                return TRUE;
            }
			}
		}
    }
	 public function addNewStore1($filename,$bannerfile, $uploadData) {
    	$tags =$_POST['tags'][0];
       $multipletag = explode(",", $tags);
       $addtag='';
       for ($i=0;count($multipletag)>$i;$i++) {
       $addtag .= " ".$multipletag[$i];
       }
		//create store code
        $date = date_create();
        $data = array(
            'userid' => $this->session->userdata('userid'),
            'categoryid' => $this->categoryid = $_POST['categoryid'],
             'subcategoryid' => $this->subcategoryid = $_POST['subcategoryid'],
            'name' => $this->name = $_POST['name'],
            'email' => $this->email = $_POST['email'],
            'mobile' => $this->mobile = $_POST['mobile'],
			'country'=>$this->country = $_POST['country'],
			'state'=>$this->country = $_POST['state'],
			'city'=>$this->country = $_POST['city'],
			'area'=>$this->country = $_POST['area'],
			'address'=>$this->address = $_POST['address'],
            'description' => $this->description = $_POST['description'],
			//'tags'=>$addtag,
            'firstimage' => $this->firstimage = $filename,
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
        );
        $this->db->insert('stores', $data);
        $id = $this->db->insert_id();
		if($this->session->userdata('plan')==1)
				$this->entryPlan(1,$id);
		elseif($this->session->userdata('plan')==4)
				$this->platinumPlan(4,$id,$bannerfile);	
		else
				$this->entryPlanWithoutPay(1,$id);	
        
		//multiple file upload code here
        if ($id) {
            if ($uploadData != '') {
                for ($j = 0; $j < count($uploadData); $j++) {
                    $data1 = array(
                        'images' => $this->images = $uploadData[$j]['file_name'],
                        'storeid' => $this->storeid = $id,
                        'createdat' => date('Y-m-d H:m:s'),
                        'updatedat' =>date('Y-m-d H:m:s'),
                    );
                    $this->db->insert('stroe_images', $data1);
                }
                return TRUE;
            } else {
                return TRUE;
            }
        }
    }
	public function platinumPlan($plan,$storeid,$bannerfile)
	{	
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		
		$data=array(
					'storeid'=>$storeid,
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		$bannerData=array('storeid'=>$storeid,'images'=>$bannerfile,'IsActive'=>1);
		
		$this->db->insert('banner', $bannerData);
		$this->session->unset_userdata('plan');
		return $this->db->insert('store_payment', $data);
		
	}
	public function platinumPlanB($plan,$storeid,$bannerfile)
	{	

		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		
		$data=array(
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
	
		$bannerData=array('storeid'=>$storeid,'images'=>$bannerfile,'IsActive'=>1);
		$this->db->insert('banner', $bannerData);
		$this->session->unset_userdata('plan');
		return $this->db->where('storeid',$storeid)->update('store_payment', $data);
	}
	public function entryPlan($plan,$storeid)
	{
		
			$occDate=date('Y-m-d');
			$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
			$nextDate=date('Y-'.$forOdNextMonth.'-d');
			
			$data=array(
						'storeid'=>$storeid,
						'paymentid'=>$plan,
						'startdate'=>date('Y-m-d'),
						'enddate'=>$nextDate,
						'IsActive'=>1,
						'Paid'=>1,
						'timestamp'=>date('Y-m-d H:m:s'),
			);
			$this->session->unset_userdata('plan');
			$this->db->insert('store_payment', $data);
			$payment_id=$this->db->insert_id();
			$this->session->set_userdata('payment_id',$payment_id);
			$this->session->unset_userdata('payment');
			return true;
	}
	public function entryPlanWithoutPay($plan,$storeid)
	{
		
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		$data=array(
					'storeid'=>$storeid,
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>0,
					'Paid'=>0,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		$this->session->unset_userdata('plan');
		return $this->db->insert('store_payment', $data);
	}
	public function getStoreInfo()
	{
		return $this->db->get('stores')->result_array();
	}
	public function getStoreData($id)
	{
		return $this->db->where('id',$id)->get('stores')->row();
	}
	public function getMenu()
	{
		return $this->db->get('categories')->result_array();
	}
	public function getStoreImages($id)
	{
		return $this->db->where('storeid',$id)->get('stroe_images')->result_array();
	}
	public function getStoresInfoByCategory($id)
	{
		if(($this->db->where('categoryid',$id)->get('stores')->num_rows()) > 0)
		{
			
			return $this->db->where('categoryid',$id)->get('stores')->result_array();
		}
		else {
			return "no recode found !!!";
		}
	}
	public function getCities($state_id)
	{

		return  $this->db->where('state_id',$state_id)->get('cities')->result_array();
	}
	public function getStateData($country_id)
	{
		   return  $this->db->where('country_id',$country_id)->get('states')->result_array();
	}
	public function getCountry()
	{
		return $this->db->get('countries')->result_array();
	}
	public function get_mysqli()
	{
		$db = (array)get_instance()->db;
		return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
	}
	public function getStoresInfoBySearchBox($city,$search,$area)
	{
		$query=explode(" ",$search);
		$records=array();
			if(!empty($city) && !empty($query) && !empty($area))
			{
				return $this->ifNotNullSearch($city,$query,$area);
			}
			elseif(empty($city) && !empty($query) && !empty($area))
			{
				return $this->ifCityNull($query,$area);
			}
			elseif(empty($city) && !empty($query) && empty($area))
			{
				
				return $this->ifAreaNull($query);
			}
			elseif(!empty($city) && !empty($query) && empty($area))
			{
				return $this->ifAreaNull1($city,$query);
			}
	}
	public function ifAreaNull1($city,$query)
	{
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1' and city='$city' and  ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1' and city='$city' and  ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and city='$city'  and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and city='$city'  and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function ifAreaNull($query)
	{
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1'   and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1'  and  ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function ifNotNullSearch($city,$query,$area)
	{
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1' and city='$city' and area='$area' and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1' and city='$city' and area='$area' and ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and city='$city' and area='$area'  and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and city='$city' and area='$area'   and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function ifCityNull($query,$area)
	{
				$condition='';
				for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1' and  area='$area' and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1'  and area='$area' and ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and area='$area' and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and area='$area' and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function getNumStore()
	{
		return  $this->db->where('userid',$this->session->userdata('userid'))->get('stores')->num_rows();
	}
    public function getStores(){
		$userid=$this->session->userdata('userid');
		$this->db->select('stores.userid,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->get()->result_array();	
			
	}
	public function getAdminStores()
	{
		$userid=$this->session->userdata('userid');
		$this->db->select('stores.userid,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->where('stores.adminid',$userid)->get()->result_array();	
	
	}
	public function updateUserView($id,$viewCount)
	{
		$totalview=$this->db->select('user_clicks')->where('id',$id)->get('stores')->row();
		$count=$totalview->user_clicks + $viewCount;
		$data=array(
		'user_clicks'=>$count,
		);
		$this->db->where('id', $id);
		return $this->db->update('stores', $data);
	}
	public function paymentPay($id,$store_id)
	{
		
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		$data=array(
					'paymentid'=>$id,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		$this->session->unset_userdata('store_id');
		return $this->db->where('storeid',$store_id)->update('store_payment', $data);
	}
	public function getCalenderData()
	{
		$this->db->select('*');
		$this->db->from('calender');
		$this->db->join('month','month.id=calender.monthID');
		return $this->db->where('calender.IsActive','1')->get()->result_array();
	}
	public function getCountData()
	{
		return $this->db->get('manage_store_payment')->result();
	}
	public function storedelete($id) {
        $this->db->where('id', $id);
        $this->db->delete('stores');
        $this->db->where('storeid', $id);
        return $this->db->delete('store_payment');
    }
	//check this store availabel in session or not
	public function checkSession($storeid,$userid)
	{
		$adminid=$this->session->userdata('userid');
		$row=$this->db->where('id',$storeid)->where('userid',$userid)->where('adminid',$adminid)->get('stores')->num_rows();
		if($row == 1)
			return true;
		else
			return false;
	}
	//when addstore for existing user
	public function getPrevStoreDetails($storeid)
	{
			$data=$this->db->where('id',$storeid)->get('stores')->row();
			if($data->subcategoryid != 'other')
			{
				$this->db->select('*,categories.name as categoriesname,subcategories.name as subcategoriesname,stores.name as storesname');
				$this->db->from('stores');
				$this->db->join('categories','categories.id=stores.categoryid');
				$this->db->join('subcategories','subcategories.id=stores.subcategoryid');
				return $this->db->where('stores.id',$storeid)->get()->result_array();
			}
			else
			{
				$this->db->select('*,categories.name as categoriesname,stores.name as storesname');
				$this->db->from('stores');
				$this->db->join('categories','categories.id=stores.categoryid');
				$data=$this->db->where('stores.id',$storeid)->get()->result_array();
				$othercat=$this->db->where('storeid',$storeid)->get('othercategory')->row();
				if(!empty($othercat->name))
				{
					$data['othercat']=$othercat->name;
					return $data;
				}
				else
				{
					$data['othercat']='';
					return $data;
				}
			}			
	}
	//when add new store for new user
	public function getPrevStoreData($id)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('userdetails','userdetails.userid=user.id');
		$user=$this->db->where('user.id',$id)->get()->result_array();
		$data=array();
		foreach($user as $row)
		{
			$this->db->select('*,categories.name as categoriesname,subcategories.name as subcategoriesname,stores.name as storesname');
			$this->db->from('stores');
			$this->db->join('categories','categories.id=stores.categoryid');
			$this->db->join('subcategories','subcategories.id=stores.subcategoryid');
			$store=$this->db->where('stores.userid',$id)->get()->result_array();
			if(empty($store))
			{
			$store=array();
			$store[0]['id']="";
			$store[0]['categoryid']="";
			$store[0]['subcategoryid']="";
			$store[0]['description']=" ";
			$store[0]['email']="";
			$store[0]['mobile']="";
			$store[0]['area']="";
			$store[0]['city']="";
			$store[0]['address']="";
			$store[0]['categoriesname']="";
			$store[0]['subcategoriesname']="";
			$store[0]['storesname']="";
			}
			
			$data[]=array('userdata'=>$row,
						'storedata'=>$store,
						);
		}
		return $data;
	}
	public function saveUserDetails()
	{
		
		if ($this->session->userdata('newStoreUserID')) 
		{
				$this->session->set_userdata('firstemail',$_POST['useremail']);
            $data = array(
                'username' => $this->name = $_POST['username'],
                'email' => $this->email = $_POST['useremail'],
                'mobile' => $this->mobile = $_POST['mobile'],
                'usertype' =>2,
                'isactive' => 0,
            );
            $this->db->where('id', $this->session->userdata('newStoreUserID'));
             $this->db->update('user', $data);
			 
			  $data = array(
               
                'first_name' => $this->name = $_POST['fname'],
                'last_name' => $this->name = $_POST['lname'],
                'email' => $this->email = $_POST['useremail'],
                'mobile' => $this->mobile = $_POST['mobile'],
					);
			$this->db->where('userid',$this->session->userdata('newStoreUserID'));
            return $this->db->update('userdetails', $data);
        } else 
		{
			
			
            $data = array(
                'username' => $this->name = $_POST['username'],
                'email' => $this->email = $_POST['useremail'],
                'mobile' => $this->mobile = $_POST['mobile'],
                'password' => md5($_POST['username'].'@123'),
                'usertype' =>2,
                'isactive' => 0,
            );
			
			
            if ($this->db->insert('user', $data)) 
			{
                $id = $this->db->insert_id();
                $data = array(
                'userid' => $this->name = $id,
                'first_name' => $this->name = $_POST['fname'],
                'last_name' => $this->name = $_POST['lname'],
                'email' => $this->email = $_POST['useremail'],
                'mobile' => $this->mobile = $_POST['mobile'],
					);
					
				$storesdata=array(
								'userid'=>$id,
								'createdat'=>date('Y-m-d H:m:s'),
								'updatedat'=>date('Y-m-d H:m:s'),
								'email'=>$_POST['useremail'],
								'mobile'=>$_POST['mobile'],
								);	
				
				$this->db->insert('userdetails',$data);	
					
				
				$this->session->set_userdata('newStoreUserID', $id);
				
                return true;
            }
			else
			{
                return false;
            }
        }
	}
	public function saveStoreDetails()
	{
		
		if ($this->session->userdata('newStoreID')) 
		{
			if (!empty( $this->session->userdata('newStoreUserID'))) 
			{
				$userid =$this->session->userdata('newStoreUserID');
            }else
			{
                $userid=$this->session->userdata('vendorID');
            }
			if($_POST['subcategoryid']=='other')
				{
					$row=$this->db->where('id',$this->session->userdata('newStoreID'))->get('othercategory')->num_rows();
					if($row == 1)
					{
						$othercat=array('userid'=>$userid,'name'=>$_POST['other'],'creatdat'=>date('Y-m-d'),'updatedat'=>date('Y-m-d'));
						$this->db->where('storeid',$this->session->userdata('newStoreID'))->update('othercategory',$othercat);
					}
					else
					{
						$othercat=array('userid'=>$userid,'storeid'=>$this->session->userdata('newStoreID'),'name'=>$_POST['other'],'creatdat'=>date('Y-m-d'),'updatedat'=>date('Y-m-d'));
						$this->db->insert('othercategory',$othercat);
					}
				}
				else{
					$this->db->where('storeid',$this->session->userdata('newStoreID'))->delete('othercategory');
				}
				
            $data = array(
                'userid' => $userid,
                'categoryid' => $this->categoryid = $_POST['categoryid'],
                'subcategoryid' => $this->subcategoryid = $_POST['subcategoryid'],
                'name' => $this->name = $_POST['name'],
                'email' => $this->email = $_POST['email'],
                'mobile' => $this->mobile = $_POST['mobile'],
                'city' => $this->country = $_POST['city'],
                'area' => $this->country = $_POST['area1'],
                'description' => $this->description = $_POST['description'],
                'address' => $this->description = $_POST['address'],
                'landline' => $_POST['landline'],
				
            );
            $this->db->where('id', $this->session->userdata('newStoreID'));
            return $this->db->update('stores', $data);
        } 
		else 
		{	
             
            if (!empty( $this->session->userdata('newStoreUserID'))) 
			{
				$userid =$this->session->userdata('newStoreUserID');
            }else
			{
                $userid=$this->session->userdata('vendorID');
            }
	
			$data = array(
                'userid' => $userid,
                'categoryid' => $this->categoryid = $_POST['categoryid'],
                'subcategoryid' => $this->subcategoryid = $_POST['subcategoryid'],
                'name' => $this->name = $_POST['name'],
                'email' => $this->email = $_POST['email'],
                'mobile' => $this->mobile = $_POST['mobile'],
                'city' => $this->country = $_POST['city'],
                'area' => $this->country = $_POST['area1'],
                'description' => $this->description = $_POST['description'],
                'address' => $this->description = $_POST['address'],
                'landline' => $_POST['landline'],
				'adminid'=>$this->session->userdata('userid'),
            );	
             
            if ($this->db->insert('stores', $data)) 
			{
                $id = $this->db->insert_id();	
				if($_POST['subcategoryid']=='other')
				{
					$othercat=array('userid'=>$userid,'storeid'=>$id,'name'=>$_POST['other'],'creatdat'=>date('Y-m-d'),'updatedat'=>date('Y-m-d'));
					$this->db->insert('othercategory',$othercat);
				}
				$this->session->set_userdata('newStoreID',$id);
                return true;
            }else 
			{
                return false;
            }
        }
	}
	public function editStore($id)
	{
		$subcat=$this->db->where('id',$id)->get('stores')->row();
		
		if($subcat->subcategoryid=='other')
		{
				$this->db->select('*,categories.name as categoriesname,stores.name as storesname');
				$this->db->from('stores');
				$this->db->join('categories','categories.id=stores.categoryid');
				$data=$this->db->where('stores.id',$id)->get()->result_array();
				$othercat=$this->db->where('storeid',$id)->get('othercategory')->row();
				
				if(!empty($othercat->name))
				{
					$data['othercat']=$othercat->name;
					return $data;
				}
				else
				{
					$data['othercat']='';
					return $data;
				}
		}
		else
		{
			$this->db->select('*,categories.name as categoriesname,subcategories.name as subcategoriesname');
			$this->db->from('stores');
			$this->db->join('categories','categories.id=stores.categoryid');
			$this->db->join('subcategories','subcategories.id=stores.subcategoryid');
			return $this->db->where('stores.id',$id)->get()->result_array();
		}	
			
				
					
	}

	public function save_images()
	{
	    $img_folder = 'storeimages/';
		
		if ($this->session->userdata('newStoreID')) 
		{
		$data = array(
                'firstimage'=> $img_folder.$this->input->post('img')
               );
			   
            $this->db->where('id', $this->session->userdata('newStoreID'));
           $this->db->update('stores', $data);
		   
		    $store_id =$this->session->userdata('newStoreID');
				 
				 $store_multi_imgs = json_decode($this->input->post('store_multi_imgs'));
            //print_r($store_multi_imgs);die();

            /*remove previous store images*/
            $delete = $this->db->where('storeid', $store_id)
                               ->delete('stroe_images'); 
					
            if($store_multi_imgs != NULL)
                {
                    foreach($store_multi_imgs as $img)
                    {
                        /* insert new images*/
                        $data = array(
                            'storeid' => $store_id,
                            'images' => $img_folder . $img,
                            'updatedat' => date('Y-m-d H:m:s')
                        );

                        $this->db->insert('stroe_images', $data);
                    }
                    //echo "Updated both tables";
                    return true;
                }
            
			
		    return true;
        } 
		else 
		{	
            $data = array(
                'firstimage'=> $img_folder.$this->input->post('img')
               );
			   
            if ($this->db->insert('stores', $data)) 
			{
                $id = $this->db->insert_id();
                $this->session->set_userdata('newStoreID', $id);
				 
				 $store_id =$this->session->userdata('newStoreID');
				 
				 $store_multi_imgs = json_decode($this->input->post('store_multi_imgs'));
            //print_r($store_multi_imgs);die();

            /*remove previous store images*/
            $delete = $this->db->where('storeid', $store_id)
                               ->delete('stroe_images');

            if($delete)
            {
                if($store_multi_imgs != NULL)
                {
                    foreach($store_multi_imgs as $img)
                    {
                        /* insert new images*/
                        $data = array(
                            'storeid' => $store_id,
                            'images' => $img_folder . $img,
                            'updatedat' => date('Y-m-d H:m:s')
                        );

                        $this->db->insert('stroe_images', $data);
                    }
                    //echo "Updated both tables";
                    return true;
                }
            }
        
				
                return true;
            } 
			else 
			{
                return false;
            }
        }
	}
	public function  adminEntryPlan($plan)
	{
			$date = date_create();
			$data = array(
            'userid' => $this->session->userdata('newStoreUserID'),
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
			'adminid'=>$this->session->userdata('userid'),
			);
        $this->db->where('id', $this->session->userdata('newStoreID'));
         $this->db->update('stores', $data);
		 
		 $this->db->set('isactive','1')->where('id',$this->session->userdata('newStoreUserID'))->update('user');
		 $id=$this->session->userdata('newStoreID');
			$this->session->unset_userdata('newStoreID');
			$this->session->unset_userdata('newStoreUserID');
			$this->session->unset_userdata('firstpage');
			
		if($plan==1)
				$this->entryPlan(1,$id);
		else
				$this->entryPlanWithoutPay(1,$id);
			
					$data1 ['email'] = $_POST['useremail'];
                    $data1 ['username'] =$_POST['username'];
                    $data1 ['password'] = $_POST['username'].'@123';

                    $this->load->library('email');
                    $config = array(
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'wordwrap' => TRUE,
                        'priority' => '1'
                    );
                    $this->email->initialize($config);
                    $this->email->from('abhishek@trigensoft.com', 'Quickfinder');
                    $this->email->to($data1['email']);
                    $this->email->subject('Activate your store ' .$data1['username'].',');
                    $body = $this->load->view('emailtemplate/storeadd', $data1, TRUE);
                    $this->email->message($body);
                    if ($this->email->send())
					{
                         return TRUE;
                    }
				
	}
	public function adminPlatinumPlan($plan)
	{
		$date = date_create();
			$data = array(
            'userid' => $this->session->userdata('newStoreUserID'),
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
			'adminid'=>$this->session->userdata('userid'),
			);
        $this->db->where('id', $this->session->userdata('newStoreID'));
        $this->db->update('stores', $data);
		$this->db->set('isactive','1')->where('id',$this->session->userdata('newStoreUserID'))->update('user');
		$id=$this->session->userdata('newStoreID');
		if($this->session->userdata('payment_id'))
		{
			$payment_id=$this->session->userdata('payment_id');
			return $this->updatePaymentPay($plan,$payment_id);	
		}
		else
		{
			return $this->entryPlan($plan,$id);	
		}			
	}
	public function updatePaymentPay($plan,$payment_id)
	{
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		$data=array(
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		return $this->db->where('id',$payment_id)->update('store_payment', $data);
	}
	public function adminSubmitPlatinumPlan()
	{
			
			$this->session->unset_userdata('newStoreID');
			$this->session->unset_userdata('newStoreUserID');
			$this->session->unset_userdata('firstpage');
			$this->session->unset_userdata('payment');
			$this->session->unset_userdata('payment_id');
			$this->session->unset_userdata('paymentType');
					
			$data1 ['email'] = $_POST['useremail'];
			$data1 ['username'] =$_POST['username'];
			$data1 ['password'] = $_POST['username'].'@123';

			$this->load->library('email');
			$config = array(
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => TRUE,
				'priority' => '1'
				);
			$this->email->initialize($config);
			$this->email->from('abhishek@trigensoft.com', 'Quickfinder');
			$this->email->to($data1['email']);
			$this->email->subject('Activate your store ' .$data1['username'].',');
			$body = $this->load->view('emailtemplate/storeadd', $data1, TRUE);
			$this->email->message($body);
			if ($this->email->send())
				{
					return TRUE;
				}
				else
				{
					return false;
				}			
	}
	
}

?>