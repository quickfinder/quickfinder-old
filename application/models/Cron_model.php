<?php
class Cron_model extends CI_Model {
	public function check_Status()
	{
		$todayDate=date('Y-m-d');
		$data=$this->db->select('id,storeid,startdate,enddate')->where('IsActive',1)->get('banner')->result_array();
		foreach($data as $row)
		{
			if($todayDate > $row['enddate'] )
			{
				$this->db->set('IsActive',0)->where('id',$row['id'])->update('banner');
			}
		}
		return true;
	}
}
?>