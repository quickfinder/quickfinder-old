<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signIn');
		}
		$this->load->model('User_model');
		$this->load->model('Store_model');
	}	
	public function index()
	{
		
		$data['user']=$this->User_model->getUser();
		$data['page'] = 'users/userlist';
        $this->load->view('templates/content', $data);
		$this->load->view('users/user', $data);
	}
	public function addUser()
	{
		$this->form_validation->set_rules('fname', 'firstname', 'required');
        $this->form_validation->set_rules('lname', 'lastname', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email');
        $this->form_validation->set_rules('mobile', '', 'required');
        $this->form_validation->set_rules('password', '', 'required');
        $this->form_validation->set_rules('address', '', 'required');
        if ($this->form_validation->run() == FALSE)
		{
			$data['js'] = 'js/passwordstatus.js';
            $data['css'] = 'css/passwordstatus.css';
			$data['page'] = 'users/adduser';
			$this->load->view('templates/content', $data);
			$this->load->view('js/js', $data);
        }
		else
		{
			if($this->User_model->addUser())
			{
				redirect('Users');
			}
        }
	}
	public function viewstore($id)
	{
		$data['storelist'] = $this->User_model->getAdminStores($id);
        $data['page'] = 'store/adminstore';
        $this->load->view('templates/content', $data);
	}
	public function vendorStore($id)
	{
		$data['storelist'] = $this->User_model->getVendorStore($id);
        $data['page'] = 'store/vendorstore';
        $this->load->view('templates/content', $data);
	}
	public function vendor()
	{
		$data['user']=$this->User_model->getVendors();
		$data['page'] ='users/vendorlist';
        $this->load->view('templates/content', $data);
		$this->load->view('users/user', $data);
	}
	public function userlist()
	{
		$data['user']=$this->User_model->getUsers();
		$data['page'] ='users/userlist';
        $this->load->view('templates/content', $data);
	}
	public function updateUser($id)
	{
		$this->form_validation->set_rules('fname', 'firstname', 'required');
        $this->form_validation->set_rules('lname', 'lastname', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email');
        $this->form_validation->set_rules('mobile', '', 'required');
        $this->form_validation->set_rules('address', '', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$data['user']=$this->User_model->getAdminDetails($id);
			$data['state']=$this->Store_model->getStateData(173);
			$data['page'] ='users/updateuser';
			$this->load->view('templates/content', $data);
        }
		else
		{
			if($this->User_model->updateAdminDetails($id))
			{
				$this->session->set_userdata('success',1);
				redirect('userlist');
			}
			
		}
	}
	public function updateadmin($id)
	{
		$this->form_validation->set_rules('fname', 'firstname', 'required');
        $this->form_validation->set_rules('lname', 'lastname', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email');
        $this->form_validation->set_rules('mobile', '', 'required');
        $this->form_validation->set_rules('address', '', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$data['user']=$this->User_model->getAdminDetails($id);
			$data['state']=$this->Store_model->getStateData(173);
			$data['page'] ='users/updateadmin';
			$this->load->view('templates/content', $data);
        }
		else
		{
			if($this->User_model->updateAdminDetails($id))
			{
				$this->session->set_userdata('success',1);
				redirect('Users');
			}
			
		}
	}
	
	public function admindelete()
	{
		if($this->User_model->admindelete())
		{
			echo json_encode(['sucess'=>'1']);
		}
	}
	public function back()
	{
		$this->session->unset_userdata('success');
		redirect('Users');
	}
	public function backtouser()
	{
		$this->session->unset_userdata('success');
		redirect('userlist');
	}
	public function backtovendorlist()
	{
		redirect('vendorslist');
	}
	public function updatevendor($id)
	{
		$this->form_validation->set_rules('fname', 'firstname', 'required');
        $this->form_validation->set_rules('lname', 'lastname', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email');
        $this->form_validation->set_rules('mobile', '', 'required');
        $this->form_validation->set_rules('address', '', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$data['user']=$this->User_model->getAdminDetails($id);
			$data['state']=$this->Store_model->getStateData(173);
			$data['page'] ='users/updatevendor';
			$this->load->view('templates/content', $data);
			
        }
		else
		{
			if($this->User_model->updateAdminDetails($id))
			{
				$this->session->set_userdata('success',1);
				redirect('vendorslist');
			}
			
		}
	}
}
?>