<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

    public function __construct() {
        parent::__construct();
		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signIn');
		}
        $this->load->model('Store_model');
        $this->load->model('Category_model');
    }

    public function index() 
	{
        $data['storelist'] = $this->Store_model->getStores();
		if($num_store=$this->Store_model->getNumStore())
		{
			$this->session->set_userdata('num_store',$num_store);
		}
        $data['page'] = 'store/storelist';
        $this->load->view('templates/content', $data);
    }
	public function adminstore() {
        $data['storelist'] = $this->Store_model->getAdminStores();
        $data['page'] = 'store/adminstore';
        $this->load->view('templates/content', $data);
    }
    public function newStore() {
		echo "string";
        die;
        $data['categorylist'] = $this->Category_model->getCategory();
        $data['country'] = $this->Store_model->getCountry();
		$data['cal']=$this->Store_model->getCalenderData();
		$data['page'] = 'store/demoaddstore';
        $this->load->view('templates/content', $data);
    }

	public function editstore($id)
	{
	    /*
	     * set store id in session to access
	     */
	    $this->session->set_userdata('storeid',$id);
	    //echo "Store_id ".$this->session->userdata('storeid');die();

		$this->form_validation->set_rules('name', '', 'required');
        $this->form_validation->set_rules('categoryid', '', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email');
        $this->form_validation->set_rules('mobile', '', 'required');
        $this->form_validation->set_rules('city', '', 'required');
        $this->form_validation->set_rules('area1', 'area', 'required');
        $this->form_validation->set_rules('description', '', 'required');
        $this->form_validation->set_rules('address', '', 'required');
        if ($this->form_validation->run() == FALSE)
		{
			if($data['storeData']=$this->Store_model->editStore($id))
			{
				$data['categorylist'] = $this->Category_model->getCategory();
				$data['cal']=$this->Store_model->getCalenderData();

				//echo "<pre>";print_r($data);echo "</pre>";die();

				$data['page'] = 'store/editstore.php';
				$this->load->view('templates/content', $data);
				// $this->load->view('store/js',$data);
				/*
				 * load page specific js
				 */
                // $this->load->view('js/dropzone-update-cover-img');
				
                // $this->load->view('js/dropzone-update-store-imgs');
                // $this->load->view('js/form');

			}
        }
		else
		{
           echo json_encode(['sucess'=>'1']);
        }
	}


    function getStoreCoverImage()
    {
        if($this->session->userdata('storeid'))
		{
		  $store_id = $this->session->userdata('storeid');	
		}
        else if ($this->session->userdata('newStoreID'))
		{
		   $store_id  = $this->session->userdata('newStoreID'); 
		}

        $store_info= $this->Store_model->getStoreCoverImg($store_id);
        //print_r($store_info);die();

		if($store_info != NULL)
		{
        $folder = FCPATH."storeimages/";

        $imgs = array();
        foreach($store_info as $store)
        {
            //echo "store img :".$store['firstimage'];
            preg_match("/[^\/]+$/",$store['firstimage'],$matches);
            $cover_page_img = $matches[0]; // get image name only
            array_push($imgs,$cover_page_img);
        }

        //print_r($imgs);

        foreach($imgs as $file)
        { //get an array which has the names of all the files and loop through it
            $obj['name'] = $file; //get the filename in array
            $obj['size'] = filesize($folder.$file); //get the flesize in array
            $result[] = $obj; // copy it to another array
        }

        //header('Content-Type: application/json');
        echo json_encode($result);
	  } 
       return null;
	}


    public function getStoreMultiImg()
    {
        if($this->session->userdata('storeid'))
		{
		  $store_id = $this->session->userdata('storeid');	
		}
        else if ($this->session->userdata('newStoreID'))
		{
		   $store_id  = $this->session->userdata('newStoreID'); 
		}

        $multiimgs = $this->Store_model->get_store_multi_images($store_id);
		if($multiimgs != NULL)
		{
        $folder = FCPATH."storeimages/";
        $imgs = array();
        foreach($multiimgs as $store)
        {
            //echo "store img :".$store['firstimage'];
            preg_match("/[^\/]+$/",$store['images'],$matches);
            $cover_page_img = $matches[0]; // get image name only
            array_push($imgs,$cover_page_img);
        }

        //print_r($imgs);

        foreach($imgs as $file)
        { //get an array which has the names of all the files and loop through it
            $obj['name'] = $file; //get the filename in array
            $obj['size'] = filesize($folder.$file); //get the flesize in array
            $result[] = $obj; // copy it to another array
        }

        //header('Content-Type: application/json');
        echo json_encode($result);

	  }
	  return null;
    }
	
	public function updateStore()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";
        $insert = $this->Store_model->updateStoreData();
        redirect("storelist");
    }
    public function adminaddnewstores()
	{

		$data['categorylist'] = $this->Category_model->getCategory();
        $data['country'] = $this->Store_model->getCountry();
		$data['cal']=$this->Store_model->getCalenderData();
		$data['count']=$this->Store_model->getCountData();
		if($this->session->userdata('newStoreUserID'))
		{
            echo "string";
			$data['store']=$this->Store_model->getPrevStoreData($this->session->userdata('newStoreUserID'));
			$this->session->set_userdata('firstpage','2');
			$data['page'] = 'store/adminaddoldstore';
			$this->load->view('templates/content', $data);	
			$this->load->view('js/dropzone-add-cover-img');
            $this->load->view('js/dropzone-add-store-imgs');
            $this->load->view('js/form');
		}
		else
		{	
			$this->session->set_userdata('firstpage','1');
		    $data['page'] = 'store/adminaddnewstore';
			$this->load->view('templates/content', $data);
			 $this->load->view('js/dropzone-add-cover-img');
             $this->load->view('js/dropzone-add-store-imgs');
             $this->load->view('js/form');
		}
	}
	public function adminnewStore($userid) 
	{

		if($this->session->userdata('newStoreID'))
		{
				
				if($this->Store_model->checkSession($this->session->userdata('newStoreID'),$userid))
				{

					$data['store']=$this->Store_model->getPrevStoreDetails($this->session->userdata('newStoreID'));
					$data['categorylist'] = $this->Category_model->getCategory();
					$data['country'] = $this->Store_model->getCountry();
					$data['cal']=$this->Store_model->getCalenderData();
					$data['page'] = 'store/adminaddstoreusers';
					$this->load->view('templates/content', $data);
					// $this->load->view('store/js',$data);
				}
				else
				{
					$this->session->set_userdata('vendorID',$userid);
					$data['categorylist'] = $this->Category_model->getCategory();
					$data['country'] = $this->Store_model->getCountry();
					$data['cal']=$this->Store_model->getCalenderData();
					$data['page'] = 'store/adminaddstore';
					$this->load->view('templates/content',$data);
					// $this->load->view('store/js',$data);
				}
			
		}else
		{
			$this->session->set_userdata('vendorID',$userid);
			$data['categorylist'] = $this->Category_model->getCategory();
			$data['country'] = $this->Store_model->getCountry();
			$data['cal']=$this->Store_model->getCalenderData();
			$data['page'] = 'store/adminaddstore';
			$this->load->view('templates/content',$data);
			// $this->load->view('store/js',$data);
		}
    }
	 public function newStore1() {
        $data['categorylist'] = $this->Category_model->getCategory();
        $data['country'] = $this->Store_model->getCountry();
		$data['page'] = 'store/addstorewithbanner1';
        $this->load->view('templates/content', $data);
    }
    public function getState(){
        
        $country_id = $_POST['country_id'];
        $states = $this->Store_model->getStateData($country_id);
        if(count($states) > 0){

            $state_box='';
            $state_box.='<option>Select states</option>';

            foreach ($states as $state) {
                $state_box.='<option id="'.$state['id'].'" value="'.$state['name'].'">'.$state['name'].'</option>';
            }
            echo json_encode($state_box);
        }

    }
	 public function getSubCategory(){
      
        $categoryid = $_POST['categoryid'];
        $subcategory = $this->Store_model->getSubCategory($categoryid);
		
        if(count($subcategory) > 0){

            $state_box='';
            $state_box.='<option>Select Subcategory</option>';

            foreach ($subcategory as $row) {
                $state_box.='<option id="'.$row['id'].'" value="'.$row['id'].'">'.$row['name'].'</option>';
            }
			$state_box.='<option  value="other"> Other</option>';
            echo json_encode($state_box);
        }

    }
	public function storedelete()
	{
		if($this->Store_model->storedelete($_POST['id']))
		{
			echo json_encode(['sucess'=>'1']);
		}
		else{
			echo json_encode(['sucess'=>'2']);
		}
	}
	public function getCity(){

        $state_id = $_POST['state_id'];
        // print_r($_POST);
        // die;
        $cities = $this->Store_model->getCities($state_id);
        if(count($cities) > 0){

            $city_box='';
            $city_box.='<option>Select city</option>';

            foreach ($cities as $city) {
                $city_box.='<option value="'.$city['name'].'" id="'. $city['id'] .'">'.$city['name'].'</option>';
            }
            echo json_encode($city_box);
        }

    }

    public function addNewStore() {

       
		$filesCount = count($_FILES['images']['name']);
		//multiple image upload code
        $uploadData= array();
        for ($i = 0; $i < $filesCount; $i++) 
		{
            $_FILES['image']['name'] = $_FILES['images']['name'][$i];
            $_FILES['image']['type'] = $_FILES['images']['type'][$i];
            $_FILES['image']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['image']['error'] = $_FILES['images']['error'][$i];
            $_FILES['image']['size'] = $_FILES['images']['size'][$i];
            $uploadPath = './storeimages/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $fileData = $this->upload->data();
				$uploadData[$i]['file_name'] = 'storeimages/' . $fileData['file_name'];
            }
        }
		//create store with single image upload code
        // Upload the files then pass data to your model
        $config['upload_path'] = './storeimages/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('img')) {
            // If the upload fails
            echo $this->upload->display_errors('<p>', '</p>');
        } else {
            // Pass the full path and post data to the set_newstudent model
            $filedata = $this->upload->data();
            $filename = 'storeimages/' . $filedata['file_name'];
            if ($this->Store_model->addNewStore($filename, $uploadData)) {
                redirect('Store');
            }
        }
    }
	//admin add new stores for vendor without vender id
	public function admin_store()
    {
		$filesCount = count($_FILES['images']['name']);
        $uploadData= array();
        for ($i = 0; $i < $filesCount; $i++) 
		{
            $_FILES['image']['name'] = $_FILES['images']['name'][$i];
            $_FILES['image']['type'] = $_FILES['images']['type'][$i];
            $_FILES['image']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['image']['error'] = $_FILES['images']['error'][$i];
            $_FILES['image']['size'] = $_FILES['images']['size'][$i];
            $uploadPath = './storeimages/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $fileData = $this->upload->data();
				$uploadData[$i]['file_name'] = 'storeimages/' . $fileData['file_name'];
            }
        }
		//create store with single image upload code
        // Upload the files then pass data to your model
        $config['upload_path'] = './storeimages/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('img')) {
            // If the upload fails
            echo $this->upload->display_errors('<p>', '</p>');
        } else {
            // Pass the full path and post data to the set_newstudent model
            $filedata = $this->upload->data();
            $filename = 'storeimages/' . $filedata['file_name'];
            if ($this->Store_model->admin_store($filename, $uploadData)) {
                redirect('Store');
            }
        }
	}
	public function adminaddnewstore() {

       
		$filesCount = count($_FILES['images']['name']);
		//multiple image upload code
        $uploadData= array();
        for ($i = 0; $i < $filesCount; $i++) 
		{
            $_FILES['image']['name'] = $_FILES['images']['name'][$i];
            $_FILES['image']['type'] = $_FILES['images']['type'][$i];
            $_FILES['image']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['image']['error'] = $_FILES['images']['error'][$i];
            $_FILES['image']['size'] = $_FILES['images']['size'][$i];
            $uploadPath = './storeimages/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $fileData = $this->upload->data();
				$uploadData[$i]['file_name'] = 'storeimages/' . $fileData['file_name'];
            }
        }
		//create store with single image upload code
        // Upload the files then pass data to your model
        $config['upload_path'] = './storeimages/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('img')) {
            // If the upload fails
            echo $this->upload->display_errors('<p>', '</p>');
        } else {
            // Pass the full path and post data to the set_newstudent model
            $filedata = $this->upload->data();
            $filename = 'storeimages/' . $filedata['file_name'];
            if ($this->Store_model->adminaddNewStore($filename, $uploadData)) {
                redirect('Store');
            }
        }
    }
	public function addNewStoreP() {

       
		$filesCount = count($_FILES['images']['name']);
		//multiple image upload code
        $uploadData= array();
        for ($i = 0; $i < $filesCount; $i++) {

            $_FILES['image']['name'] = $_FILES['images']['name'][$i];
            $_FILES['image']['type'] = $_FILES['images']['type'][$i];
            $_FILES['image']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['image']['error'] = $_FILES['images']['error'][$i];
            $_FILES['image']['size'] = $_FILES['images']['size'][$i];

            $uploadPath = './storeimages/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $fileData = $this->upload->data();
				$uploadData[$i]['file_name'] = 'storeimages/' . $fileData['file_name'];
            }
        }
		//upload banner
		
        if ($this->upload->do_upload('banner')) {
		$banner = $this->upload->data();
		
		$bannerfile = 'storeimages/' . $banner['file_name'];
		
		//create store with single image upload code
        // Upload the files then pass data to your model
        $config['upload_path'] = './storeimages/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

			if (!$this->upload->do_upload('img')) {
				// If the upload fails
				echo $this->upload->display_errors('<p>', '</p>');
			} else {
				// Pass the full path and post data to the set_newstudent model
				$filedata = $this->upload->data();
				
				$filename = 'storeimages/' . $filedata['file_name'];
				if ($this->Store_model->addNewStore1($filename,$bannerfile, $uploadData)) {
					redirect('Store');
				}
			}
		}
    }
	public function pricing()
	{
		 $data['page'] = 'pricing/pricing';
         $this->load->view('templates/content', $data);
	}
	public function pricingp($id)
	{
		 $this->session->set_userdata('store_id',$id);
		 $data['page'] = 'pricing/pricingpayment';
         $this->load->view('templates/content', $data);
	}
	public function plan($id)
	{
		$this->session->set_userdata('paymentType',$id);
		if(!$this->session->userdata('newStoreID'))
		{
				$this->session->set_userdata('plan',$id);
				if($id==4)
				{
					$this->newStore1();
				}
				else
				{
					$this->newStore();
				}
		}
		else
		{
			if($id==1)
			{
				if($this->Store_model->adminEntryPlan($id))
					echo json_encode(['sucess'=>'1','plan'=>$id]);
				else
					echo json_encode(['sucess'=>'2']);
			}
			elseif($id==4)
			{
				if($this->Store_model->adminPlatinumPlan($id))
					echo json_encode(['sucess'=>'1','plan'=>$id]);
				else
					echo json_encode(['sucess'=>'2']);
			}
		}
	}
	public function adminSubmitPlatinumPlan()
	{
		if($this->Store_model->adminSubmitPlatinumPlan())
		{
			$this->session->set_userdata('success',1);
			echo json_encode(['sucess'=>'1']);
		}
		else{
			echo json_encode(['sucess'=>'2']);
		}
	}
	public function planP($id)
	{
		if($id==4)
		{
			$this->session->set_userdata('plan',4);
			$data['page'] = 'pricing/addBanner';
            $this->load->view('templates/content', $data);
		}
		else
		{
			$store_id= $this->session->userdata('store_id');
			if($this->Store_model->paymentPay($id,$store_id))
			{
				redirect('Store');
			}
		}
	}
	public function planB($id)
	{
		$uploadPath = './storeimages/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
		$store_id= $this->session->userdata('store_id');
		echo $store_id;
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('banner'))
		{
			
			$banner = $this->upload->data();
			
			$bannerfile = 'storeimages/' . $banner['file_name'];
			$store_id= $this->session->userdata('store_id');
			
			if($this->Store_model->paymentPay($id,$store_id))
			{
				if($this->Store_model->platinumPlanB($id,$store_id,$bannerfile))
				{
					$this->session->unset_userdata('plan');
					redirect('Store');
				}
			}
		}
	}

}

?>