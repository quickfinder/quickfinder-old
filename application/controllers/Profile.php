<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signIn');
		}
		$this->load->model('Profile_model');
		$this->load->model('Validation_model');
	}	
	public function index()
	{
		$this->form_validation->set_rules('fname', 'firstname', 'trim|required');
        $this->form_validation->set_rules('lname', 'lastname', 'trim|required');
        $this->form_validation->set_rules('username', '', 'trim|required|callback_useremail_check');
        $this->form_validation->set_rules('email', '', 'trim|required|valid_email|callback_editemail_check');
        if ($this->form_validation->run() == FALSE) {
			$data['user']=$this->Profile_model->getUserData();
			$this->session->set_userdata('prevEmail',$data['user']->email);
			$this->session->set_userdata('prevUsername',$data['user']->username);
			$data['page'] = 'users/profile';
			$this->load->view('templates/content', $data);
		}
		else
		{
			
			$config['upload_path'] = './profilePic/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 100;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $this->load->library('upload', $config);
			if(!empty($_FILES['input-file-preview']['name']))
			{
				
				if (!$this->upload->do_upload('input-file-preview')) 
				{
					$error = array('error' => $this->upload->display_errors());
					$data['error'] = $error;
					$data['user']=$this->Profile_model->getUserData();
					$this->session->set_userdata('prevEmail',$data['user']->email);
					$this->session->set_userdata('prevUsername',$data['user']->username);
					$data['page'] = 'users/profile';
					$this->load->view('templates/content', $data);
				} 
				else 
				{
					$filedata = $this->upload->data();
					$filename = 'profilePic/' . $filedata['file_name'];
					$this->session->set_userdata('profilepic',$filename);
					if ($this->Profile_model->editProfile($filename)) {
						$this->session->set_userdata('success',1);
						redirect('Dashboard');
					}
				}
			}
			else
			{
				
				if ($this->Profile_model->editProfile1()) 
					{
						$this->session->set_userdata('success',1);
						redirect('Dashboard');
					}
			}
		}
	}
	public function useremail_check($str) {
        if ($this->Validation_model->check_username_profile($str))
            return true;
        else {
            $this->form_validation->set_message('useremail_check', 'The {field} allready exists ');
            return false;
        }
    }
	 public function editemail_check($str) {
        if ($this->Validation_model->editCheck_email($str))
            return true;
        else {
            $this->form_validation->set_message('editemail_check', 'The {field} allready exists ');
            return false;
        }
    }
	public function changePassword()
	{
		$this->form_validation->set_rules('pwd', 'old password', 'trim|required|callback_password_check');
        $this->form_validation->set_rules('npwd', 'New password', 'required');
        $this->form_validation->set_rules('cpwd', 'Confirm password', 'required|matches[npwd]');
        if ($this->form_validation->run() == FALSE) {
            $data['page'] = 'users/changepassword';
			$this->load->view('templates/content', $data);
        } else 
		{
            if ($this->Profile_model->changePassword()) {
                $this->session->set_userdata('success',1);
						redirect('Dashboard');
            }
        }
	}
	public function password_check($str) {
        if ($this->Validation_model->password_check($str))
            return true;
        else {
            $this->form_validation->set_message('password_check', 'The {field} Not exists ');
            return false;
        }
    }
}
?>