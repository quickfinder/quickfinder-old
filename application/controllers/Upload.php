<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Store_model');
    }

    public function upload_img()
	{
		$ds = DIRECTORY_SEPARATOR;  //1
		 
		$storeFolder = 'storeimages/';   //2
		 
		if (!empty($_FILES)) {
			 
			$tempFile = $_FILES['file']['tmp_name'];          //3             
			  
			//echo $tempFile;
			
			//$targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4
			
            $targetPath = FCPATH. $ds. $storeFolder;  //4
			 
			$randomId = uniqid();
			//echo $randomId;
			
			/***append random id to fileanme***/
			$newfilename= trim($randomId . $_FILES['file']['name']);
			
			$targetFile =  trim($targetPath.$newfilename) ;  //5
		 
			$file_exists = file_exists ( $targetFile );
			
			if( !$file_exists ) //If file does not exists then upload
			{
				move_uploaded_file($tempFile,$targetFile); //6
					 
				$response=array();
					 
				$response=array(
					   'filename' =>$newfilename
				);
					 
				echo json_encode($response);   
			 }
		}
	}
	

    /*
     * delete file from uploded folder
     */
    public function delete_img()
	{ 
      if (isset($_POST) && $_POST['filename']) 
	{
	  unlink(FCPATH.'admin/storeimages/'.$_POST['filename']);
		
	  $response =array(
	   'success' => 1,
	   'msg' => " File : ".$_POST['filename']." deleted sucessfully"
	   );
	}
	else
	{
	  $response =array(
		'success' => 0,
		'msg' => " File : ".$_POST['filename']." cannot be deleted"
	   );
	}

	echo json_encode($response);

	}

    /*
     * delete file from uploded folder
     */
    public function delete_store_multi_img()
	{ 
      if (isset($_POST) && $_POST['filename']) 
	{
	  unlink(FCPATH.'storeimages/'.$_POST['filename']);
		
      $delete= $this->Store_model->db_delete_store_multi_img($_POST['filename']);
        
	  $response =array(
	   'success' => 1,
	   'msg' => " File : ".$_POST['filename']." deleted sucessfully"
	   );
	}
	else
	{
	  $response =array(
		'success' => 0,
		'msg' => " File : ".$_POST['filename']." cannot be deleted"
	   );
	}

	echo json_encode($response);

	}

	public function get_file_list()
	{
		/*
		 * get list of all files
		 * folders are skipped by using  && is_numeric($pos) 
		 */
		$folder = FCPATH."storeimages/";

		$imgs = array();

		if ($handle = opendir($folder.'.')) {

			while (false !== ($entry = readdir($handle))) {
				
				$pos = strpos( $entry, '.' );
				
				if ($entry != "." && $entry != ".." && is_numeric($pos) ) 
				{
				 //   echo "<br>$entry\n";
					array_push($imgs,$entry);
				}
			}

			closedir($handle);
		}

		//print_r($imgs);

		foreach($imgs as $file)
		{ //get an array which has the names of all the files and loop through it 
			$obj['name'] = $file; //get the filename in array
			$obj['size'] = filesize($folder.$file); //get the flesize in array
			$result[] = $obj; // copy it to another array
		}

		header('Content-Type: application/json');
		echo json_encode($result); 
	}
}

?>