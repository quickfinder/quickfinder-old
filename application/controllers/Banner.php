<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Banner_model');
    }

    public function listBanner() {
        $data['bannerlist'] = $this->Banner_model->getBannerData();
        $data['page'] = 'banner/bannerlist';
        $this->load->view('templates/content', $data);
		$this->load->view('banner/js', $data);
    }

    public function addBanner() 
	{
        $data['page'] = 'banner/addbanner';
        $this->load->view('templates/content', $data);
    }
    public function addNewBanner() 
	{

        $config['upload_path'] = './bannerimages/';
        $config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG';   
		$config['max_size'] = 1024*100;
        $config['max_width'] = 1024000;
        $config['max_height'] =1000000;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('input-file-preview')) 
		{
            $error = array('error' => $this->upload->display_errors());
            $data['error'] = $error;
            $data['page'] = 'banner/addbanner';
            $this->load->view('templates/content', $data);
        }else
		{
            $filedata = $this->upload->data();
            $filename = 'bannerimages/' . $filedata['file_name'];
            if ($this->Banner_model->insertBanner($filename)) {
                redirect('banner-list');
            }
        }
    }
	public function viewEditForm($id)
	{
		$data['banner']=$this->Banner_model->getDetails($id);
		$data['page'] = 'banner/editbanner';
        $this->load->view('templates/content', $data);
			
	}
	public function editBanner($id)
	{
		$config['upload_path'] = './bannerimages/';
        $config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG';
        $config['max_size'] = 1024*100;
        $config['max_width'] = 1024000;
        $config['max_height'] =1000000;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('input-file-preview')) 
		{
            $error = array('error' => $this->upload->display_errors());
			$data['banner']=$this->Banner_model->getDetails($id);
            $data['error'] = $error;
            $data['page'] = 'banner/editbanner';
            $this->load->view('templates/content', $data);
        } else {
            $filedata = $this->upload->data();
            $filename = 'bannerimages/' . $filedata['file_name'];
            if ($this->Banner_model->editBanner($filename,$id)) {
                $this->session->set_userdata('success',1);
				redirect('banner-list');
            }
        }
	}
	public function activeBanner($id)
	{
		if($this->Banner_model->activeBanner($id))
		{
			$this->session->set_userdata('success',1);
			redirect('banner-list');
		}
	}
	public function deactiveBanner($id)
	{
		if($this->Banner_model->deactiveBanner($id))
		{
			$this->session->set_userdata('success',1);
			redirect('banner-list');
		}
	}
	public function bannerdelete()
	{
		if($this->Banner_model->bannerdelete($_POST['id']))
		{
			echo json_encode(['sucess'=>'1']);
		}
	}
	public function PlatinumPlan()
	{
		$monthid=$_POST['month'];
		$year=$_POST['year'];
		$store_id=$this->session->userdata('newStoreID');
		if($this->Banner_model->addPlatinumPlan($monthid,$year,$store_id))
		{
			echo json_encode(['sucess'=>'1']);
		}
	}
	

}

?>