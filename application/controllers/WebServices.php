<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class WebServices extends CI_Controller 
{
    public function __construct()
    {  
        parent::__construct();
        $this->load->model('WebServices_model');
        $this->load->model('Category_model');
        $this->load->model('Search_model');
        $this->load->model('Rating_model');
        $this->load->model('Register_model');
    }   
    public  function  get_banner_list()
    {  
        $banners = $this->Search_model->getBanner();
          /*
        echo "<pre>";
        print_r($banners);
        echo "</pre>";die();
        */
  
        $data =array();
        foreach ($banners as $banner)
        {
            $folder ="admin/";

            array_push($data,array(
                "imgname"=>$banner['images'],
                "path"=>base_url().$folder.$banner['images']  //base_url().'images/slider/'
            ));
        }
        //print_r($response);

        $response = array(
            "success" =>array(
                'code'=>200,
                'message'=>'success'
            ),
            "data"=>$data
        );

        echo json_encode($response,true);
    }
    public function get_category_list(){
        $category = $this->WebServices_model->getCategories();

        
        // echo "<pre>";
        // print_r($category);
        // echo "</pre>";die();
                $data =array();
        foreach ($category as $category)
        {
            $folder ="admin/";

            array_push($data,array(
                "id"=>$category['id'],
                "name"=>$category['name'],
                "icon_name"=>$category['icon_name'],
                "IsActive"=>$category['IsActive'],
                "path"=>base_url().$folder.$category['icons']  //base_url().'images/slider/'
            ));
        }
        // print_r($response);

        $response = array(
            "success" =>array(
                'code'=>200,
                'message'=>'success'
            ),
            "data"=>$data
        );

        echo json_encode($response,true);
        


    }

public function userSearch(){
     // print_r($_REQUEST);
    // die;
                   $data =array();
        $search=$_REQUEST['search'];

        $city = $_REQUEST['city'];
        $area = $_REQUEST['area'];

                if($userinfo = $this->WebServices_model->getSearch($city,$area,$search)){
                // $userinfo= $userinfo1['row'];
// print_r($userinfo);
// die;
foreach ($userinfo as $userinfo)
        {
  
            array_push($data,array(
                                "row"=>$userinfo['row'],

                // "id"=>$userinfo['id'],
                // "address"=>$userinfo['address'],
                // "firstimage"=>$userinfo['firstimage'],
                // "storename"=>$userinfo['storename'],
                // "email"=>$userinfo['email'],
                // "mobile"=>$userinfo['mobile'],
                "rating"=>$userinfo['rating']

            ));
        }
$response = array(
            "success" =>array(
                'message'=>'All data'
            ),
            "data"=>$data
        );
                echo json_encode($response,true);

}else{

  $response = array(
            "success" =>array(
                'message'=>'No data.'
            ),
            "data"=>$data
        );

}
        echo json_encode($response,true);

}


//change pwd
public function changePwdUser() {
      
        $oldpwd = $_REQUEST['opwd'];
        $newpwd = $_REQUEST['npwd'];
        $cpwd = $_REQUEST['cpwd'];
        $userid = $_REQUEST['userid'];

           if ($userinfo=$this->WebServices_model->changePwdUser($oldpwd,$newpwd,$cpwd,$userid)) {
            
                $response = array(
            "success" =>array(
             "message"=>"Password Change Sussecfully!.."
            )
            // "data"=>$data
        );
            }else {
                // echo "string";
             $response = array(
            "success" =>array(
                'message'=>'Please try agin.'
            )
            // "data"=>$data
        );
            }
                    echo json_encode($response,true);

        
    }
//login
  public function userLogin(){
    // print_r($_REQUEST);
    // die;
                   $data =array();

        $username = $_REQUEST['username'];
        $password = $_REQUEST['password'];
    if ($userinfo=$this->WebServices_model->userLoginDetails($username,$password)) {
        // echo "hii";
        
       // $userinfo = $this->WebServices_model->userLoginDetails($username,$password);

        
        // echo "<pre>";
        // print_r($userinfo);
        // echo "</pre>";
        // die();
                $data =array();
        foreach ($userinfo as $userinfo)
        {

            array_push($data,array(
                "id"=>$userinfo['id'],
                "username"=>$userinfo['username'],
                "usertype"=>$userinfo['usertype'],
                "mobile"=>$userinfo['mobile'],
                "email"=>$userinfo['email'],
                "registrationid"=>$userinfo['registrationid'],
                "islogin"=>$userinfo['islogin'],
                "isactive"=>$userinfo['isactive'],
                "isnew"=>$userinfo['isnew'],
                "first_name"=>$userinfo['first_name'],
                "last_name"=>$userinfo['last_name'],
                "userid"=>$userinfo['userid']

            ));
        }
         $response = array(
            "success" =>array(
        "message"=>"login Successfully"
            ),
            "data"=>$data
        );

        //             $data['username'] = $username;
        //             $data['password'] = $password;
        //         array_push($data,array(
        //         "message"=>"login Successfully"
        //                 ));

        //     $response = array(

        //             "success" =>array(
        //         'message'=>'success'
        //     ),
        //     "data"=>$data
        // );

}else{
// echo "string";
    $response = array(
            "errormsg" =>array(
                'message'=>'Invalid Username and password.'
            ),
            "data"=>$data
        );

}
        echo json_encode($response,true);

}


//forgot password
public function forgotPassword()
    {

        $email = $_REQUEST['email'];
        if ($id =$this->WebServices_model->checkUserEmailId($email)) {
                        
                    $encryptedID = base64_encode($id->id);

                    $data1 ['id'] = $encryptedID;

                    $data1 ['email'] = $_REQUEST['email'];
                    $this->load->library('email');
                    $config = array(
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'wordwrap' => TRUE,
                        'priority' => '1'
                    );
					
                    $this->email->initialize($config);
                    $this->email->from('abhishek@trigensoft.com', 'Quickfinder');
                    $this->email->to($_REQUEST['email']);
                    $this->email->subject('Forgot Password from Quickfinder ');
                    $body = $this->load->view('emailtemplate/forgotmail', $data1, TRUE);
                    $this->email->message($body);
                    if ($this->email->send()) 
					{
                        //array_push($data,array("message"=>"Send Link for Forgot Password Plz Check check your mail."
                        //));
					 $response = array(
								"success" =>array('message'=>'success'),
								"data"=>"Send Link for Forgot Password Plz Check check your mail.",
							);
							echo json_encode($response);
                    }
                    else{
                          $response = array(
							"errormsg" =>array(
								'message'=>'Invalid Email! Try again..'
							),
							"data"=>$data
						);
						echo json_encode($response);
						} 
            
                    }
        else
        {
            $response = array(
            "errormsg" =>array(
                'message'=>'Invalid Email!..'
            ),
            "data"=>$data
        );
echo json_encode($response);
                
        }
        
    }

//registration form
  public function getRegistration(){

        $fname = $_REQUEST['fname'];
        $lname = $_REQUEST['lname'];
        $email = $_REQUEST['email'];
        $mobile = $_REQUEST['mobile'];
        $username = $_REQUEST['username'];
        $password = $_REQUEST['password'];


      

            if ($this->WebServices_model->checkUniqueEmail($email)) {
            if ($this->WebServices_model->checkUniqueUserName($username)) {

                if ($id = $this->WebServices_model->userRegister($fname,$lname,$email,$mobile,$username,$password)) 
                {
                    $data1 ['email'] = $email;
                    $data1 ['username'] = $username;
                    $data1 ['id'] = base64_encode($id);
// print_r($email);
// die;
               $data =array();
 
                    $this->load->library('email');
                    $config = array(
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'wordwrap' => TRUE,
                        'priority' => '1'
                    );
                    $this->email->initialize($config);
                    
                    $this->email->from('abhishek@trigensoft.com', 'Quickfinder');
                    $this->email->to($email);
                    $this->email->subject('Activate Your Account ' . $username.',');
                    $body = $this->load->view('emailtemplate/confirmationmail', $data1, TRUE);
                    $this->email->message($body);

                    if ($this->email->send()) {

                        array_push($data,array(
                "message"=>"Register Successfully! Check you email and confirm your account.."
                        ));

 $response = array(
            "success" =>array(
                'message'=>'success'
            ),
            "data"=>$data
        );

                    }
                    else{

                          $response = array(
            "errormsg" =>array(
                'message'=>'Register not successfully! Try again..'
            ),
            "data"=>$data
        );
            } 



                    }
                   
                   }else{
   $response = array(
            "errormsg" =>array(
                'message'=>'User name already exits! Try again..'
            ),
            "data"=>$data
        );



}
                   } else {
        // $this->session->set_userdata('errmsg', "Email allready Exists!..");
        // $this->load->view('signUp');
            $response = array(
            "errormsg" =>array(
                'message'=>'Email allready Exists!..'
            ),
            "data"=>$data
        );

    }



        echo json_encode($response,true);

  }

    public function getSubcategoryList($id){
        $subcategory = $this->WebServices_model->getSubcategories($id);

        
        // echo "<pre>";
        // print_r($subcategory);
        // echo "</pre>";die();
                $data =array();
        foreach ($subcategory as $subcategory)
        {
            $folder ="admin/";

            array_push($data,array(
                "id"=>$subcategory['id'],
                "name"=>$subcategory['name'],
                "category_id"=>$subcategory['category_id']

            ));
        }
        // print_r($response);

        $response = array(
            "success" =>array(
                'code'=>200,
                'message'=>'success'
            ),
            "data"=>$data
        );

        echo json_encode($response,true);
        


    }


 public function getStoreprofileList($id){
    // print_r($id);
$data['storeprofile'] = $this->Category_model->getStoreProfile($id);
        $data['storerating'] = $this->Category_model->getStoreRating($id);
        $info = $this->WebServices_model->userDetailsInfo($id);
        $overallrating = $this->WebServices_model->getStoreRatingOverall($id);
        
        // echo "<pre>";
        // print_r($data['storeprofile']);
        // echo "</pre>";die();
       $profileimg= $data['storeprofile'];
                       // $rows=$data['storerating'];
                       // $row=$rows[0]['user_details'];
 $count=count($overallrating);
 $value=$overallrating[$count-1]['value'];
 $poor=$overallrating[$count-1]['poor'];
 $avg=$overallrating[$count-1]['average'];
 $good=$overallrating[$count-1]['good'];
 $verygood=$overallrating[$count-1]['verygood'];
 $excellence=$overallrating[$count-1]['excellence'];

$rating= ceil($value/$count);
// echo $rating;
 print_r($overallrating['user_details']);
// die;
                $data =array();
                $data1 =array();
                $data2 =array();
                $data3 =array();
               array_push($data1,array(

                "storeid"=>$profileimg[0]['storeid'],
                "mobile"=>$profileimg[0]['mobile'],
                "landline"=>$profileimg[0]['landline'],
                "area"=>$profileimg[0]['area'],
                "city"=>$profileimg[0]['city'],
                "state"=>$profileimg[0]['state'],
                "country"=>$profileimg[0]['country'],
                "address"=>$profileimg[0]['address']


            ));
                foreach ($profileimg as $profileimg) {
            $folder ="admin/";
                                array_push($data,array(
                 "path"=>base_url().$folder.$profileimg['images']  //base_url().'images/slider/'

            ));
                }
// print_r($data1);
        foreach ($overallrating as $rows)
        {
            // print_r($rows);
            $folder ="admin/";

                       $row=$rows['user_details'];
                       // print_r($row); 
            array_push($data2,array(
                "rating"=>$rows['rating'],
                "review"=>$rows['review'],
                "userid"=>$rows['userid'],
                   
                "info"=>$rows['user_details']
            ));
        }

        
            array_push($data3,array(        
                "value"=>$value,
                "value"=>$value,

                "poor"=>$poor,
                "average"=>$average,
                "good"=>$good,
                "verygood"=>$verygood,
                "excellence"=>$excellence
            ));
        
        // print_r($response);

        $response = array(
            "success" =>array(
                'code'=>200,
                'message'=>'success'
            ),
            "images"=>$data,
            "data"=>$data1,
            "rating"=>$data3,
            "ratinginfo"=>$data2


        );

        echo json_encode($response,true);
        


    }


      public function getStoreList($id,$subid){
        // print_r($id);
        // die;
        $storelist = $this->WebServices_model->getStore($id,$subid);

         // $data['storeprofile'] = $this->Category_model->getStoreProfile($id,$subid);
        // $data['storerating'] = $this->Category_model->getStoreRating($decode);

        // echo "<pre>";
        // print_r($storelist);
        // echo "</pre>";
        // die();
                $data =array();
                $row =array();
        foreach ($storelist as $storelist)
        {
            $folder ="admin/";

               $rows=$storelist['row'];
            array_push($data,array(
            // array_push($row,array(
                "user_clicks"=>$rows['user_clicks'],
                "address"=>$rows['address'],
                "id"=>$rows['id'],
                // "firstimage"=>$rows['firstimage'],
                 "path"=>base_url().$folder.$rows['firstimage'],  //base_url().'images/slider/'

                "storename"=>$rows['storename'],
                "email"=>$rows['email'],
                "mobile"=>$rows['mobile'],
                "name"=>$rows['name'],
                "payment_type"=>$rows['payment_type'],
                "IsActive"=>$rows['IsActive'],


                "rating"=>$storelist['rating']

            ));
        }
        // print_r($rows);

        $response = array(
            "success" =>array(
                'code'=>200,
                'message'=>'success'
            ),
            "data"=>$data
        );

        echo json_encode($response,true);
        


    }
}
  