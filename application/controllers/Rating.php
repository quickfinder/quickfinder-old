<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Rating_model');
    }

    public function submitRating($id) {
        $decode = base64_decode($id);
        if ($this->Rating_model->insertRating($decode)) {
            redirect('storeprofile/' . $id);
        }
    }

    public function ratingLogin($id) {
        $decode = base64_decode($id);

		$response = array();
		$data = $this->Rating_model->ratingLogin();
		
		//print_r($_POST);
		
        if ($data) {
			$userdata = array(
                'userid' => $data->userid,
                'usertype' => $data->usertype,
                'email' => $data->email,
                'is_logged_in' => true,
                'is_logged' => 1,
            );
            $this->session->set_userdata($userdata);
            //redirect('storeprofile/' . $id);
			
			$response = array(
		     'success'=>1,
			 'title' => 'Sucess',
			 'msg' => '',
			 'redirect_loc' => base_url().'storeprofile/' . $id
			);
        } 
		else 
		{
		   $response = array(
		     'success'=>0,
			 'title' => 'Error!... authentication failed',
			 'msg' => 'Please provide corrent authentication details'
			);
			
           //return false;
        }
		echo json_encode($response);
		//echo "Hi";
    }

}
