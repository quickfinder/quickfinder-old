<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Register_model');
		$this->load->model('store_model');
		$this->load->model('Validation_model');
	}	
	public function index()
	{
		
		$data['page'] = 'home';
		$this->load->view('templates/content',$data);
	}
	public function login()
	{

		
		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			
			$this->load->view('signIn');
		}
		else
		{
			redirect('dashboard');
		}
		
	}
	public function userLogin()
	{

		$this->form_validation->set_rules('username', '', 'required');
		$this->form_validation->set_rules('password', '', 'required');
		if($this->form_validation->run() == FALSE)
          {
				$this->load->view('signIn');
          }
         else
            {
               if($data=$this->Register_model->login())
				{
					if(!empty($data[0]['profilepic']))
				{
					$this->session->set_userdata('profilepic',$data[0]['profilepic']);
				}
				else{
					$this->session->set_userdata('profilepic','asset/img/avatar.jpg');
				}
					$this->session->set_userdata(array('userid'=>$data[0]['id'],'usertype'=>$data[0]['usertype'],'email'=>$data[0]['email'],'is_logged_in'=>TRUE,'is_logged'=>1));
					
					if (REQUEST == "external")
						{ 
							return;
						} else {
 
					//Do other Things
 
						}
					if($num_store=$this->store_model->getNumStore())
					{
						$this->session->set_userdata('num_store',$num_store);
					}
					redirect('dashboard');
				}
				else
				{
					$this->session->set_userdata('errmsg','UserName And Password Missmatch!...');
					$this->load->view('signIn');
				}
            }
	}
	public function viewRegister()
	{
		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signUp');
			
		}
		else
		{
			redirect('dashboard');
		}
	}
	public function forgotPassword()
	{
        $this->form_validation->set_rules('username', 'username', 'required|callback_useremail_check');
        if ($this->form_validation->run() == FALSE)
		{	
			$this->load->view('forgotpwd');		
        }
		else
		{
			if ($id = $this->Register_model->checkEmailID()) 
				{
					$encryptedID = base64_encode($id->id);
					
					$data1 ['email'] = $_POST['username'];
                    $data1 ['id'] = $encryptedID;
                    $this->load->library('email');
                    $config = array(
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'wordwrap' => TRUE,
                        'priority' => '1'
                    );
                    $this->email->initialize($config);
                    $this->email->from('abhishek@trigensoft.com', 'Quickfinder');
                    $this->email->to($_POST['username']);
                    $this->email->subject('Forgot Password from Quickfinder ');
                    $body = $this->load->view('emailtemplate/forgotmail', $data1, TRUE);
                    $this->email->message($body);

                    if ($this->email->send()) {

                        $this->session->set_userdata('msg', "Send Link for Forgot Password Plz check your mail.");
                        $this->load->view('forgotpwd');
                    }
            }
		}
		
	}
	public function userforgotpwd($id)
	{
		
		$userid = base64_decode($id);
		
		$this->form_validation->set_rules('newpassword', '', 'required');
		$this->form_validation->set_rules('confirmpassword', '', 'required|matches[newpassword]');
        if ($this->form_validation->run() == FALSE)
		{	
			$data['js']='js/passwordstatus.js';
			$data['css']='css/passwordstatus.css';
			$this->load->view('changepassword');
			$this->load->view('js/js',$data);
        }
		else
		{
			if($this->Register_model->forgotchangepassword($userid))
			{
				$this->session->set_userdata('msg', "Password change succefully you can do login!...");
				redirect('signin');
			}
		}
	}
	public function useremail_check($str)
	{
		if($this->Validation_model->check_emailForgot($str))
			return true;
		else
		{
			$this->form_validation->set_message('useremail_check', 'The {field} Missmatch!.. ');
			return false;
		}
	}
	public function Register()
	{
		$this->form_validation->set_rules('mobile', 'Plz Enter The mobile', 'required');
		$this->form_validation->set_rules('password', '', 'required|min_length[6]|max_length[15]');
		$this->form_validation->set_rules('email', 'Email', 'required');
		if($this->form_validation->run() == FALSE)
          {
                $data['page'] = 'signUp';
				$this->load->view('signUp');
          }
         else
            {
				if($this->Register_model->checkEmail())
				{
				   if($this->Register_model->register())
					{
						$this->session->set_userdata('msg',"Register Successfully!..");
						$this->load->view('signIn');
					}
					else
					{
						$this->load->view('signUp');
					}
				}
				else
				{
					$this->session->set_userdata('errmsg',"Email allready Exists!..");
					$this->load->view('signUp');
				}		
            }	
	}
	public function userRegister()
	{
		
		if($this->Register_model->register())
		{
			
			$this->load->view('signIn');
		}
		else
		{
			$data['page'] = 'signUp';
			$this->load->view('signUp');
		}
		
	}
	public function logout()
	{	
		if($this->Register_model->islogout())
		{
			$user_data = $this->session->all_userdata();
			foreach ($user_data as $key => $value) {
				if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
					$this->session->unset_userdata($key);
				}
			}
			$this->session->sess_destroy();
			$this->session->set_userdata('is_logged_in',FALSE);
			$this->session->set_userdata('is_logged',2);
			redirect('signin');
		}
	}
}
