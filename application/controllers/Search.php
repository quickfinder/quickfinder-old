<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Search_model');	
		$this->load->model('Store_model');	
	}	
	public function index()
	{
		$data['storelist'] = $this->Search_model->getStores();
		$data['page'] = 'SearchProduct/storelist';
        $this->load->view('templates/content', $data);
	}
	public function Search()
	{
		$data['storelist'] = $this->Search_model->getSearch();
		$data['page'] = 'SearchProduct/storelist';
        $this->load->view('templates/content', $data);
	}
}
?>