<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Validation_model');
        $this->load->model('Register_model');
    }
    
    // user profile
    public function userProfile()
	{
        $data['userinfo'] = $this->Register_model->profile();
        $this->load->view('user/userprofile',$data);
    }
    public function index() 
	{
        if ($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE) {
            $this->form_validation->set_rules('fname', 'Firstname', 'trim|required|min_length[4]|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('lname', 'Lastname', 'trim|required|min_length[4]|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
            $this->form_validation->set_rules('email', '', 'trim|required|valid_email|callback_useremail_check');
            $this->form_validation->set_rules('username', '', 'trim|required|callback_useremail_check');
            $this->form_validation->set_rules('password', '', 'required');
            if ($this->form_validation->run() == FALSE) {
                $data['js'] = 'js/passwordstatus.js';
                $data['css'] = 'css/passwordstatus.css';

                $this->load->view('user/signUp');
                $this->load->view('js/js', $data);
            } 
			else 
			{
                $id = $this->Register_model->userRegister();
                if ($id) 
				{
                    $to = $_POST['email']; //"nivratti@trigensoft.com";
					
                    $subject = "Verify Your Email Address, ".$_POST['fname'];

                    $datapassed['first_name'] = $_POST['fname'];
					
                    $datapassed['id'] = $id;

                    $message = $this->load->view('user/email', $datapassed, TRUE);

                    if ($this->send_mail_user($to, $subject, $message)) {
                        $this->session->set_userdata('msg', "Register Successfully! Check you email and confirm your account..");

                        //$this->load->view('user/panel');
                        redirect('signin');
                    }
                } else {
                    $this->load->view('user/signUp');
                }
            }
        } else {
            $this->load->view('user/account');
        }
    }

    //activate user account
    function verify_user_id($key) {
        $res = $this->Register_model->user_acc_activate($key);

        if ($res) {
            $this->session->set_userdata('msg', "Account Activated Successfully...");
            redirect('signin');
        } else {
            $this->session->set_userdata('msg', "You have alraedy activated your account.");
            redirect('signin');
        }
    }

    public function send_mail_user($to, $subject, $message) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.zoho.com', //'smtp.gmail.com'
            'smtp_port' => 587, //465,
            'smtp_user' => 'info@trigensoft.com',
            'smtp_pass' => '4TQiTFRcjTku',
            'smtp_crypto' => 'tls',
            'smtp_timeout' => '20',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );

        $config['newline'] = "\r\n";
        $config['crlf'] = "\r\n";
        $this->load->library('email', $config);
        $this->email->from('info@trigensoft.com', 'Quickfinder');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        //$this->email->send();
        if (!$this->email->send()) {
            return false;
        }
        return true;
    }

    public function useremail_check($str) {
        if ($this->Validation_model->check_email($str))
            return true;
        else {
            $this->form_validation->set_message('useremail_check', 'The {field} allready exists ');
            return false;
        }
    }

    public function changePassword() {
        $this->form_validation->set_rules('pwd', 'old password', 'trim|required|callback_password_check');
        $this->form_validation->set_rules('npwd', 'New password', 'required');
        $this->form_validation->set_rules('cpwd', 'Confirm password', 'required|matches[npwd]');
        if ($this->form_validation->run() == FALSE) {
            // $this->load->view('user/changePwd');
            $data['page'] = 'user/changePwd';
            $this->load->view('templates/content',$data);
        } else {
            // print_r($_POST);
            // die;
            if ($this->Register_model->changePassword()) {
                $this->session->set_userdata('msg', 'Password Change Sussecfully!..');
                redirect('panel');
            }
        }
    }  

    public function panel() {
        // $this->load->view('user/changePwd');
         $data['page'] = 'user/changePwd';
            $this->load->view('templates/content',$data);
    }

    public function password_check($str) {
        if ($this->Validation_model->password_check($str))
            return true;
        else {
            $this->form_validation->set_message('password_check', 'The {field} Not exists ');
            return false;
        }
    }

    public function profile() {
        $this->form_validation->set_rules('fname', 'firstname', 'trim|required');
        $this->form_validation->set_rules('lname', 'lastname', 'trim|required');
        $this->form_validation->set_rules('username', '', 'trim|required|callback_useremail_check');
        $this->form_validation->set_rules('email', '', 'trim|required|valid_email|callback_editemail_check');
        if ($this->form_validation->run() == FALSE) {
            $data['user'] = $this->Register_model->profile();
            $this->load->view('user/profile', $data);
        } else 
		{
            $config['upload_path'] = './profilePic/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 100;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $this->load->library('upload', $config);
			if(!empty($_FILES['input-file-preview']['name']))
			{
				if (!$this->upload->do_upload('input-file-preview')) 
				{
					$error = array('error' => $this->upload->display_errors());
					$data['user'] = $this->Register_model->profile();
					$data['error'] = $error;
					$this->load->view('user/profile', $data);
				} else 
				{
					$filedata = $this->upload->data();
					$filename = 'profilePic/' . $filedata['file_name'];
					if ($this->Register_model->editProfile($filename)) {
						$this->session->set_userdata('msg', 'profile Updated Sussecfully!..');
						redirect('my-profile');
					}
				}
			}
			else
			{
				if ($this->Register_model->editProfile1()) 
					{
						$this->session->set_userdata('msg', 'profile Updated Sussecfully!..');
						redirect('my-profile');
					}
			}
        }
    }
    public function editemail_check($str) {
        if ($this->Validation_model->editCheck_email($str))
            return true;
        else {
            $this->form_validation->set_message('editemail_check', 'The {field} allready exists ');
            return false;
        }
    }

}
