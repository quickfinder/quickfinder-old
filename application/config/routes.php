<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
//Search

$route['Search'] = 'welcome/Search';
$route['locationSearch'] = 'welcome/locationSearch';
$route['top-result'] = 'welcome/top_Result';
$route['about/(:num)'] = 'login/about/$1';
$route['Cron'] = 'Cron';
// $route['cron'] = 'Cron';
//register
$route['signup'] = 'register/viewRegister';
$route['forgotpassword'] ='register/forgotPassword';
$route['signin'] = 'register/login';
$route['signinp'] = 'register/loginp';
$route['register'] = 'register/Register';
$route['test'] = 'register/testEmail';
$route['activestatus/(:any)'] = 'register/activeStatus/$1';
$route['activestatusp/(:any)'] = 'register/activeStatusp/$1';
$route['activestatusv/(:any)'] = 'register/activeStatusv/$1';
$route['userforgotpwd/(:any)'] ='register/userforgotpwd/$1';
//Check User Name
$route['checkusername'] = 'register/checkUserName';
$route['checkuseremail'] = 'register/checkUserEmail';
//login
$route['login'] = 'register/userLogin';
$route['loginp'] = 'register/userLoginp';
//logOut
$route['logout'] = 'register/logout';
//manage store
  
$route['storelist'] = 'store';
$route['adminaddnewstores'] = 'Store/adminaddnewstores';
//manage store
$route['adminstore'] = 'Store/adminstore';
$route['editstore/(:num)'] = 'Store/editstore/$1';
$route['addstore'] = 'Store/newStore';

$route['addstore/(:num)'] = 'Store/adminnewStore/$1';

// $route['admin_store'] = 'Store/admin_store';


$route['get_cover_image'] = 'Store/getStoreCoverImage';
$route['get_store_multi_imgs'] = 'Store/getStoreMultiImg';
$route['remove_store_imgs'] = 'Store/remove_store_multi_img';
$route['addnewstore'] = 'Store/addNewStore';
$route['addnewstoreP'] = 'Store/addNewStoreP';
$route['Store/getState'] = 'Store/getState';
$route['Store/getCity'] = 'Store/getCity';
$route['Validation/addStoreNext1'] = 'Validation/addStoreNext1';
$route['Store/storedelete'] = 'Store/storedelete';
$route['update-store/([a-zA-Z0-9=]+)'] = 'Store/updateStore/$1';
$route['edit-store/(:num)'] = 'Store/editStore/$1';
$route['back'] = 'Users/back';
  

// upload images
$route['upload_img'] = 'Upload/upload_img';
$route['delete_img'] = 'Upload/delete_img';
//super admin add user(admin) 
$route['adduser'] = 'Users/addUser';
$route['updateadmin/(:num)'] = 'Users/updateadmin/$1';
$route['viewstore/(:num)'] = 'Users/viewstore/$1';
$route['vendorstore/(:num)'] = 'Users/vendorStore/$1';
$route['vendorslist'] = 'Users/vendor';
$route['userlist'] = 'Users/userlist';
$route['back'] = 'Users/back';
$route['backtouser'] = 'Users/backtouser';
$route['backtovendorlist'] = 'Users/backtovendorlist';
$route['updatevendor/(:num)'] = 'Users/updatevendor/$1';
$route['updateuser/(:num)'] = 'Users/updateUser/$1';
//banner- slider
$route['banner-list'] = 'Banner/listBanner';
$route['add-banner'] = 'Banner/addBanner';
$route['add-new-banner'] = 'Banner/addNewBanner';
$route['edit-banner/(:num)'] = 'Banner/viewEditForm/$1';
$route['editbanner/(:num)'] = 'Banner/editBanner/$1';
$route['activebanner/(:num)'] = 'Banner/activeBanner/$1';
$route['deactivebanner/(:num)'] = 'Banner/deactiveBanner/$1';
//pricing
$route['pricing'] = 'Store/pricing';
$route['pricingp/(:num)'] = 'Store/pricingp/$1';
$route['SilverPlan'] = 'Store/plan/2';
$route['EntryPlan'] = 'Store/plan/1';
$route['GoldPlan'] = 'Store/plan/3';
$route['PlatinumPlan'] = 'Store/plan/4';
$route['SilverPlanP'] = 'Store/planP/2';
$route['EntryPlanP'] = 'Store/planP/1';
$route['GoldPlanP'] = 'Store/planP/3';
$route['PlatinumPlanP'] = 'Store/planP/4';
$route['PlatinumPlanB'] = 'Store/planB/4';
//validation
$route['next1validate'] = 'Validation/addStoreNext1';
//payment
$route['paymentdetails/(:num)'] = 'payment/details/$1';
//user
$route['signupform'] = 'User';
$route['changePassword'] = 'User/changePassword';
$route['panel'] = 'User/panel';
$route['profile'] = 'User/profile';
$route['user/verify/(:any)'] = 'User/verify_user_id/$1';
//categories
$route['categories'] = 'Category';
$route['category/(:num)'] = 'Category/pages/$1';
$route['categorywise/serch'] = 'Category/serch';
$route['editcategories/(:num)'] = 'Category/editCategories/$1';
$route['backtoedicategory'] = 'Category/backtoedicategory';
//addCategory
$route['addCategory'] = 'Category/addCategory';
$route['addNewCategory'] = 'Category/addNewCategory';
$route['editCategory/(:num)'] = 'Category/editCategory/$1';
$route['deactiveCategory/(:num)'] = 'Category/deactiveCategory/$1';
$route['activeCategory/(:num)'] = 'Category/activeCategory/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//samrat
$route['my-profile'] = 'User/userProfile';

// vendor listingform
$route['listingform'] = 'FreeListing/listingForm';
$route['freelisting'] = 'FreeListing';
$route['fetch_data'] = 'welcome/fetch_all_names';
$route['fetch_areas'] = 'welcome/fetch_all_areas';
$route['changPwd/(:any)'] ='register/changPwd';
//FAVOURATES
$route['Favourite/addStore'] ='Favourite/addStore';
$route['Favourite/removeStore'] ='Favourite/removeStore';
$route['My-Favorite'] ='Favourite/myFavorite';
$route['My-Favorite/userLogin'] ='Favourite/userLogin';

/*
 * Web services
 */
/*get banner images with  base url + subfolder path*/
$route['bannerimages'] = 'WebServices/get_banner_list';
$route['ws-categorylist'] = 'WebServices/get_category_list';
$route['ws-subcategorylist/(:num)'] = 'WebServices/getSubcategoryList/$1';
$route['ws-storelist/(:num)/(:num)'] = 'WebServices/getStoreList/$1/$2';
$route['ws-storeprofile/(:num)'] = 'WebServices/getStoreprofileList/$1';
$route['ws-registration'] = 'WebServices/getRegistration';
$route['ws-login'] = 'WebServices/userLogin';
$route['ws-userforgotpassword'] ='WebServices/forgotPassword';
$route['ws-searchcity'] ='WebServices/userSearch';
$route['ws-changePwdUser'] = 'WebServices/changePwdUser';

  
$route['getsubcategory'] = 'Store/getSubCategory';
   
//Cotegory
$cat=$this->uri->segment(2);
$route['Cotegory/'.$cat] = 'Category/subCategory/'.$cat;
//subCategory
//addSubCategory
$route['addSubCategory/(:num)'] = 'Category/viewSubCategory/$1';
$route['addSubCategory/addnew'] = 'Category/addSubCategory';
$route['backtosubcategories'] = 'Category/backtosubcategories';
//manage store
$route['addSubCategory/addnew'] = 'addSubCategory/addnew';
$catsub=$this->uri->segment(1);
$route[$catsub.'/(:any)'] = 'Category/getProduct/$1';

// find stoteprofile
$route['storeprofile/(:any)'] = 'Category/storeProfile/$1';
//rating
$route['rating/(:any)'] = 'Rating/submitRating/$1';
$route['rating-login/(:any)'] = 'Rating/ratingLogin/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

