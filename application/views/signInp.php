<!DOCTYPE html>
<html lang="en">
<head>

 <meta charset="utf-8">
 
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Quickfinder</title>

  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/animate.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/icheck/skins/flat/aero.css"/>
  <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet"> 
  
  <link href="<?php echo base_url(); ?>asset/css/fade_effect_slider.css" rel="stylesheet">
  <!-- end: Css -->

  <!-- carousel slider --><link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

       <style type="text/css">
  .form-signin-wrapper {
    background: #fff !important;
}
.form-signin {
    max-width: 430px;
    padding: 10px;
    margin: 0 auto;
}
.error
{color:red;}
</style>
    </head>

    <body id="mimin" class="dashboard form-signin-wrapper">

	  <div class="form_background">
	   <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 810" aria-hidden="true"><path fill="#efefee" d="M592.66 0c-15 64.092-30.7 125.285-46.598 183.777C634.056 325.56 748.348 550.932 819.642 809.5h419.672C1184.518 593.727 1083.124 290.064 902.637 0H592.66z"></path><path fill="#f6f6f6" d="M545.962 183.777c-53.796 196.576-111.592 361.156-163.49 490.74 11.7 44.494 22.8 89.49 33.1 134.883h404.07c-71.294-258.468-185.586-483.84-273.68-625.623z"></path><path fill="#f7f7f7" d="M153.89 0c74.094 180.678 161.088 417.448 228.483 674.517C449.67 506.337 527.063 279.465 592.56 0H153.89z"></path><path fill="#fbfbfc" d="M153.89 0H0v809.5h415.57C345.477 500.938 240.884 211.874 153.89 0z"></path><path fill="#ebebec" d="M1144.22 501.538c52.596-134.583 101.492-290.964 134.09-463.343 1.2-6.1 2.3-12.298 3.4-18.497 0-.2.1-.4.1-.6 1.1-6.3 2.3-12.7 3.4-19.098H902.536c105.293 169.28 183.688 343.158 241.684 501.638v-.1z"></path><path fill="#e1e1e1" d="M1285.31 0c-2.2 12.798-4.5 25.597-6.9 38.195C1321.507 86.39 1379.603 158.98 1440 257.168V0h-154.69z"></path><path fill="#e7e7e7" d="M1278.31,38.196C1245.81,209.874 1197.22,365.556 1144.82,499.838L1144.82,503.638C1185.82,615.924 1216.41,720.211 1239.11,809.6L1439.7,810L1439.7,256.768C1379.4,158.78 1321.41,86.288 1278.31,38.195L1278.31,38.196z"></path></svg>
	  </div>
	  
	
      <div class="container" >
      <form action="<?php echo site_url('loginp');?>" class="form-signin" method="post">
          <div class="panel periodic-login" style="">
              <!-- <span class="atomic-number">28</span> -->
              <div class="panel-body text-center">
               <img class="sign_in_logo" src="<?php echo base_url();?>images/logo/1.png" alt="quickfinder logo"/>

  
                  <!-- <h1 class="atomic-symbol">Mi</h1>
                  <p class="atomic-mass">14.072110</p>
                  <p class="element-name">Miminium</p> -->

                  <i class="icons icon-arrow-down"></i><p style="color: green;
    font-size: 15px;
    font-weight: bold">
				  <?php 
				  echo $this->session->userdata('msg');
				 $this->session->unset_userdata('msg');
				  ?></p>
				  <p id="errmsg" style="color:red;
    font-size: 15px;
    font-weight: bold">
				  <?php 
				 
				   echo $this->session->userdata('errmsg');
				 $this->session->unset_userdata('errmsg');
				  ?></p>
                  <div class="form-group form-animate-text" style="margin-top:30px !important;">
                    <input type="text" class="form-text" name="username" required>
                    <span class="bar"><?php echo form_error('username', '<div class="error">', '</div>'); ?></span>
                    <label>Username</label>
                  </div>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="password" class="form-text" name="password" required>
                    <span class="bar"><?php echo form_error('password', '<div class="error">', '</div>'); ?></span>
                    <label>Password</label>
                  </div>
                  
                  <input type="submit" class="btn col-md-12" value="SignIn"/>
              </div>
                <div class="text-center" style="padding:5px;">
                   <!-- <a href="<?php echo site_url('signup');?>">Create New Acoount</a>-->
				   <a href="<?php echo site_url('signupform');?>">Create New Acoount</a>
                </div>
          </div>
        </form>

      </div>

      <!-- end: Content -->
      <!-- start: Javascript -->
       <script src="<?php echo base_url(); ?>asset/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>asset/js/jquery.ui.min.js"></script>
      <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>

      <script src="<?php echo base_url(); ?>asset/js/plugins/moment.min.js"></script>
     
      <!-- custom -->
      <script src="<?php echo base_url(); ?>asset/js/main.js"></script>
      <script type="text/javascript">
       $(document).ready(function(){
         
		 $( "input[type=text]" ).focus(function() {
		  //alert( "Handler for .focus() called." );
		  $('#errmsg').empty();
		});
       });
     </script>
     <!-- end: Javascript -->
   </body>
   </html>