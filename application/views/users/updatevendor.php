
<!-- Page header -->
        <div class="page-header page-header-default">
          <div class="page-header-content">
            <div class="page-title">
              <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Vender</span> </h4>
            </div>

            
          </div>

          <div class="breadcrumb-line">
            <ul class="breadcrumb">
              <li><a href="index.html"><i class="icon-home2 position-left"></i> Vender</a></li>
              <li class="active">Vendors Form</li>
            </ul>

            
          </div>
        </div>
        <!-- /page header -->


<!-- Content area -->
        <div class="content">
<!-- 2 columns form -->
          <form class="form-horizontal" action="#">
            <div class="panel panel-flat">
              <div class="panel-heading">
                <h5 class="panel-title">Add Admin Form</h5>
                <div class="heading-elements">
                  <ul class="icons-list">
                            <!-- <li><a data-action="collapse"></a></li> -->
                            <li><a data-action="reload"></a></li>
                            <!-- <li><a data-action="close"></a></li> -->
                          </ul>
                        </div>
              </div>

              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <fieldset>
<!--                      <legend class="text-semibold"><i class="icon-reading position-left"></i> Personal details</legend>
 -->
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Enter First name:</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" placeholder="Eugene Kopyov">
                        </div>
                      </div>

                        <div class="form-group">
                        <label class="col-lg-3 control-label">Enter Last name:</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" placeholder="Eugene Kopyov">
                        </div>
                      </div>  

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-9">
                          <input type="text" placeholder="eugene@kopyov.com" class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Mobile:</label>
                        <div class="col-lg-9">
                          <input type="text" placeholder="+918000000000" class="form-control">
                        </div>
                      </div>




  
                      
                      
                    </fieldset>
                  </div>

                  <div class="col-md-6">
                    <fieldset>
                              <!-- <legend class="text-semibold"><i class="icon-truck position-left"></i> Shipping details</legend> -->

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Select your state:</label>
                        <div class="col-lg-9">
                          <select data-placeholder="Select your state" class="select">
                            <option></option>
                            <optgroup label="Alaskan/Hawaiian Time Zone">
                              <option value="AK">Alaska</option>
                              <option value="HI">Hawaii</option>
                            </optgroup>
                            <optgroup label="Pacific Time Zone">
                              <option value="CA">California</option>
                              <option value="NV">Nevada</option>
                              <option value="WA">Washington</option>
                            </optgroup>
                            <optgroup label="Mountain Time Zone">
                              <option value="AZ">Arizona</option>
                              <option value="CO">Colorado</option>
                              <option value="WY">Wyoming</option>
                            </optgroup>
                            <optgroup label="Central Time Zone">
                              <option value="AL">Alabama</option>
                              <option value="AR">Arkansas</option>
                              <option value="KY">Kentucky</option>
                            </optgroup>
                            <optgroup label="Eastern Time Zone">
                              <option value="CT">Connecticut</option>
                              <option value="DE">Delaware</option>
                              <option value="WV">West Virginia</option>
                            </optgroup>
                          </select>
                        </div>
                      </div>

                      
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Select your city:</label>
                        <div class="col-lg-9">
                          <select data-placeholder="Select your state" class="select">
                            <option></option>
                            <optgroup label="Alaskan/Hawaiian Time Zone">
                              <option value="AK">Alaska</option>
                              <option value="HI">Hawaii</option>
                            </optgroup>
                            <optgroup label="Pacific Time Zone">
                              <option value="CA">California</option>
                              <option value="NV">Nevada</option>
                              <option value="WA">Washington</option>
                            </optgroup>
                            <optgroup label="Mountain Time Zone">
                              <option value="AZ">Arizona</option>
                              <option value="CO">Colorado</option>
                              <option value="WY">Wyoming</option>
                            </optgroup>
                            <optgroup label="Central Time Zone">
                              <option value="AL">Alabama</option>
                              <option value="AR">Arkansas</option>
                              <option value="KY">Kentucky</option>
                            </optgroup>
                            <optgroup label="Eastern Time Zone">
                              <option value="CT">Connecticut</option>
                              <option value="DE">Delaware</option>
                              <option value="WV">West Virginia</option>
                            </optgroup>
                          </select>
                        </div>
                      </div>

                      

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Address:</label>
                        <div class="col-lg-9">
                          <input type="text" placeholder="Your address of living" class="form-control">
                        </div>
                      </div>

                      
                    </fieldset>
                  </div>
                </div>

                <div class="text-right">
                 <button type="button" class="btn btn-success"><i class=" icon-arrow-left7"></i>Back </button>
                  <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                </div>
              </div>
            </div>
          </form>
          <!-- /2 columns form -->


    </div>