

<!-- Page header -->
        <div class="page-header page-header-default">
          <div class="page-header-content">
            <div class="page-title">
              <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span></h4>
            </div>

            
          </div>

          <div class="breadcrumb-line">
            <ul class="breadcrumb">
              <li><a href="index.html"><i class="icon-home2 position-left"></i> User</a></li>
              <li class="active">Update User Form</li>
            </ul>

            
          </div>
        </div>
        <!-- /page header -->
<div id="content">
    
    <div class="form-element">
	<div class="col-md-12">
                  <div class="col-md-12 panel">
                    <div class="col-md-12 panel-heading">
                      <h4> User Update Form</h4>
                    </div>
                    <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                      <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" action="<?php echo site_url('updateuser/'.$this->uri->Segment(2)); ?>">
                          
						  <div class="col-md-6">
                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" value="<?php echo $user[0]['first_name']?>" name="fname" required>
                              <span class="bar" ><?php echo form_error('fname', '<div class="error">', '</div>'); ?></span>
                              <label>Firstname</label>
                            </div>

                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" value="<?php echo $user[0]['last_name']?>" name="lname" required>
                             <span class="bar" ><?php echo form_error('lname', '<div class="error">', '</div>'); ?></span>
                              <label>Lastname</label>
                            </div>

                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="email" class="form-text" value="<?php echo $user[0]['email']?>"name="email" required>
                              <span class="bar" ><?php echo form_error('email', '<div class="error">', '</div>'); ?></span>
                              <label>Email</label>
                            </div>
							 <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" value="<?php echo $user[0]['mobile']?>" maxlength="10" pattern="[789][0-9]{9}" name="mobile" required>
                              <span class="bar" ><?php echo form_error('mobile', '<div class="error">', '</div>'); ?></span>
                              <label>Mobile</label>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                   
								   <select class="form-text states1" name="state" >
                                        <option value="<?php echo $user[0]['state']?>"><?php echo $user[0]['state']?></option>
                                        <?php
                                        foreach ($state as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>" id="<?php echo $row['id']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?> 
										
                                    </select>
									<label>Select State</label>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text cities" name="city">
                                        <option value="<?php echo $user[0]['city']?>"><?php echo $user[0]['city']?></option>
                                        <!-- <?php
                                        foreach ($city as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?> -->
                                    </select>
									<label>Select City</label>
                                </div>
							
                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" value="<?php echo $user[0]['address']?>" name="address" required>
                              <span class="bar" ><?php echo form_error('address', '<div class="error">', '</div>'); ?></span>
                              <label>Address</label>
                            </div>
							
                          </div>                   
                          <div class="col-md-12">
                            <a href="<?php echo site_url('backtouser');?>"> <input class="submit btn btn-success" type="button" value="Back"></a>
                              <input class="submit btn btn-danger" type="submit" value="Submit">
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
              </div>
        
    </div>
</div>




