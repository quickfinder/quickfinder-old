<script type="text/javascript">
	
  $(document).ready(function(){
$('.deletevendor').on('click', function() {
    		  var id=$(this).attr('id');
			  var btn = this;
			  // alert(id);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Store!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {


$.ajax({
	            url: "<?php echo site_url()?>"+'Users/admindelete',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						swal({
                    title: "Deleted!",
                    text: "Your Store has been deleted.",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
						table.row($(btn).parents('tr')).remove().draw(false);
					}
				}
					});


                
            }
            else {
                swal({
                    title: "Cancelled",
                    text: "Your  file is safe :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    });




 $(".deleteadmin").click(function(e){
			 
			  var id=$(this).attr('id');
			  var btn = this;
		 	var table = $('#datatables-example').DataTable();
			swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
				if (willDelete) {
					
				$.ajax({
	            url: "<?php echo site_url()?>"+'Users/admindelete',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						
						swal("Poof! Your Record has been deleted!", {
						icon: "success",
						});
						table.row($(btn).parents('tr')).remove().draw(false);
					}
				}
					});
					
				} 
				
            else {
                swal({
                    title: "Cancelled",
                    text: "Your  file is safe :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
				});
			
		 
		  
		 });




	 
	  
  });

</script>