
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
    <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h1><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users Profile</span> 

</h1>
                        </div>

                       
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li><a href="datatable_advanced.html">Change Password</a></li>
                          <!--   <li class="active">Change Password

</li> -->
                        </ul>

                       
                    </div>
                </div>
                <!-- /page header -->



                <!-- Content area -->
                <div class="content">

    <!-- Centered forms -->
    <div class="col-lg-12 col-md-12 col-sm-4 col-xs-4 left-group">
                    
                        <h3 style="color: green;"><b><?php echo $this->session->userdata('msg');$this->session->unset_userdata('msg');?></b></h3>
                        </div>
                    <div class="row">

                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                    <form method="post" action="<?php echo site_url('changePassword');?>">
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <h5 class="panel-title"><b>Change Password</b></h5>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <div class="form-group">
                                                    <label>Old Password:</label>
                                                    <input type="password" class="form-control" value="<?php echo set_value('pwd'); ?>" id="pwd" name="pwd" placeholder="Enter old password" required>
                              <p><?php echo form_error('pwd', '<p class="error" style="color:red;">', '</p>'); ?></p>
                                                </div>

                                                <div class="form-group">
                                                    <label>New Password:</label>
                                                   <input type="password" class="form-control" value="<?php echo set_value('npwd'); ?>" id="npwd" name="npwd" placeholder="Enter new password" required>
                              <?php echo form_error('npwd', '<p class="error" style="color:red;">>', '</p>'); ?>
                                                </div>

                                                <div class="form-group">
                                                    <label>Confirm Password:</label>
                                                    <input type="password" class="form-control" value="<?php echo set_value('cpwd'); ?>" id="cpwd" name="cpwd" placeholder="Enter confirm password" required>
                              <?php echo form_error('cpwd', '<p class="error" style="color:red;">', '</p>'); ?>
                                                </div>

                                               

                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <!-- /form centered -->
