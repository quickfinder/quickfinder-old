<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Custom Theme files -->
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!--- materialize css --->
    <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css"
          media="all"/>
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- menu style -->
    <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- banner slider -->
    <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
    <!-- carousel slider --><link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo base_url(); ?>css/circularwaves.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
    <!-- //js -->

    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>

    <!---- materialize js ----
    <script src="<php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
  -->
    <script>
        $(document).ready(function () {

            
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->


    <link href="<?php echo base_url(); ?>asset/css/fade_effect_slider.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">
<style>
.error
{color:red;}
</style>
</head>
<body>



<div class="n-ele-center n-white">
    <!-- header -->

    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="row">


                    <div class="col-lg-1 col-md-2 col-sm-6 col-xs-6">
                        <div class="header-logo">
                            <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                         src="<?php echo base_url();?>images/logo/1.png"
                                                                         alt="quickfinder logo"/></a></h1>
                        </div>
                    </div>


                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 my-acc">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 acc-heading">
                                 <h4>Free Listing</h4>
                             </div>
                         </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="social-icons">

                            <!-- Dropdown Structure -->
                            <?php if($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE){?>
                                <div class="container-fluid top">
								<a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
								<a class="bold"  href="<?php echo site_url('signupform');?>">Register</a>
                                </div>
							<?php }else{?>
							<div class="container-fluid top">
							<ul class="nav navbar-nav">
                                       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?php echo base_url();?><?php echo $this->session->userdata('profilepic');?>"/> <?php echo $this->session->userdata('email');?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('User');?>">My Account</a></li>
                                                <li><a href="<?php echo site_url('changePassword');?>">Change Password</a></li>
                                                <li><a href="<?php echo site_url('my-profile');?>">Profile</a></li>
												<?php if($this->session->userdata('usertype')==2){?> 
								<li><a href="<?php echo site_url('Dashboard');?>">Dashboard</a></li>
						<?php } ?>
												<li><a href="<?php echo site_url('logout');?>">Logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
									</div>
							<?php }?>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div><!-- //header-two -->
    </div>


    <!-- //header -->


    <div class="deals">
        <div> <!--class="container"> -->
            <div class="col-md-12">
			<form method="post" action="<?php echo site_url('listingform');?>">
			<div class="row">
				<div class="col-lg-1 col-md-4 col-sm-5 col-xs-5 left-group"></div>
                <div class="col-lg-5 col-md-4 col-sm-5 col-xs-5 left-group">
                    <div class="form-group">
							  <label for="usr">Company Name:</label>
							  <input type="text" class="form-control" value="<?php echo set_value('name'); ?>" id="name" name="name" required>
							   <?php echo form_error('name', '<p class="error">', '</p>'); ?>
					</div>
					<div class="form-group">
							  <label for="usr">Username:</label>
							  <input type="text" class="form-control"  value="<?php echo $this->session->userdata('username');?>" id="uname" name="uname" disabled>
							  <?php echo form_error('uname', '<p class="error">', '</p>'); ?>
					</div>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-5 col-xs-5 left-group">
                    <div class="form-group">
							  <label for="usr">City:</label>
							  <input type="text" class="form-control" value="Cebu" id="city"  disabled>
					</div>
					<div class="form-group">
							  <label for="usr">Email:</label>
							  <input type="text" class="form-control" value="<?php echo $this->session->userdata('email');?>" id="email" name="email" disabled>
							  <?php echo form_error('email', '<p class="error">', '</p>'); ?>
					</div>
					<button type="submit" class="btn waves-effect waves-light pull-right">
                                     <span>Submit</span>
                    </button>
                </div>
            </div>
			</form>
			</div>
        </div>
    </div>


    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
        </div>
    </div>

</div>



</body>
</html>