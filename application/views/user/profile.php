<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Custom Theme files -->
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!--- materialize css --->
    <link href="<?php echo base_url(); ?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css"
          media="all"/>
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/menu.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- menu style -->
    <link href="<?php echo base_url(); ?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- banner slider -->
    <link href="<?php echo base_url(); ?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
    <!-- carousel slider --><link rel="icon" href="<?php echo base_url(); ?>images/logo/favicon.ico" type="image/x-icon">    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo base_url(); ?>css/circularwaves.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
    <!-- //js -->

    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>

    <!---- materialize js ----
    <script src="<php echo base_url(); ?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>
  -->
    <script>
        $(document).ready(function () {

            
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->


    <link href="<?php echo base_url(); ?>asset/css/fade_effect_slider.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>asset/css/user-account.css" rel="stylesheet">
<style>
.left-group
{
      margin: auto;
    float: none;

}
.top{
	padding-top:20px;
}
.bold{
	color:#2d383e;font-weight:bold;
	padding-top:17px;
}
.navbar-nav > li > a {
    padding-top: 21px;
    padding-bottom: 15px;
}
.error{
	color:red !important;
}
.social-icons
{
	margin-top:0px !important;
}
.container{
    margin-top:20px;
}
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
.error p
{
	color:red;
}
</style>
<script>$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    });  
});</script>
</head>
<body>



<div class="n-ele-center n-white">
    <!-- header -->

    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="row">


                    <div class="col-lg-1 col-md-2 col-sm-6 col-xs-6">
                        <div class="header-logo">
                            <h1><a href="<?php echo site_url(); ?>"><img class="logo"
                                                                         src="<?php echo base_url(); ?>images/logo/1.png"
                                                                         alt="quickfinder logo"/></a></h1>
                        </div>
                    </div>


                    <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6 my-acc">
                         <div class="row">
                             <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 acc-heading">
                                 <h4>Profile</h4>
                             </div>
                             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                
                             </div>
                             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
								<?php if($this->session->userdata('usertype')==1){?> 
								
						
							   <a href="<?php echo site_url('freelisting');?>"> <button class="btn waves-effect waves-light">
                                     <span>Free Listing</span>
                                 </button>
								 </a>
								<?php } ?>
                             </div>
                         </div>

                    </div>


                    <div class="col-lg-3 col-md-2 col-sm-6 col-xs-6">
                        <div class="social-icons">

                            <!-- Dropdown Structure -->
                            <?php if($this->session->userdata('is_logged') != 1 && $this->session->userdata('is_logged_in') != TRUE){?>
                                <div class="container-fluid top">
								<a class="bold"  href="<?php echo site_url('signin'); ?>">Login</a>  /
								<a class="bold"  href="<?php echo site_url('signupform');?>">Register</a>
                                </div>
							<?php }else{?>
							<div class="container-fluid top">
							<ul class="nav navbar-nav">
                                       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?php echo base_url();?><?php echo $this->session->userdata('profilepic');?>"/> <?php echo $this->session->userdata('email');?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('User');?>">My Account</a></li>
                                                <li><a href="<?php echo site_url('changePassword');?>">change password</a></li>
                                                <li><a href="<?php echo site_url('my-profile');?>">profile</a></li>
												<?php if($this->session->userdata('usertype')==2){?> 
								<li><a href="<?php echo site_url('Dashboard');?>">Dashboard</a></li>
						<?php } ?>
												<li><a href="<?php echo site_url('logout');?>">logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
									</div>
							<?php }?>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div><!-- //header-two -->
    </div>


    <!-- //header -->


    <div class="deals">
        <div> <!--class="container"> -->
            <div class="row">
                
					
                <div class="col-lg-5 col-md-8 col-sm-4 col-xs-4 left-group">
                    
					<form method="post" action="<?php echo site_url('profile');?>" enctype="multipart/form-data">
							<div class="form-group">
							  <label for="usr">FirstName:</label>
							  <input type="text" class="form-control" value="<?php echo $user[0]['first_name']; ?>" id="fname" name="fname" required>
							   <?php echo form_error('fname', '<p class="error">', '</p>'); ?></p>
							</div>
							<div class="form-group">
							  <label for="usr">LastName:</label>
							  <input type="text" class="form-control" value="<?php echo $user[0]['last_name']; ?>" id="lname" name="lname" required>
							  <?php echo form_error('lname', '<p class="error">', '</p>'); ?>	
							</div>
							<div class="form-group">
							  <label for="usr">Mobile:</label>
							  <input type="text" class="form-control" pattern="[789][0-9]{9}" value="<?php echo $user[0]['mobile']; ?>" id="mobile" name="mobile" required>
							  <?php echo form_error('mobile', '<p class="error">', '</p>'); ?>	
							</div>
							
							
							<div class="form-group">
							  <label for="usr">User Name:</label>
							  <input type="text" class="form-control" value="<?php echo $user[0]['username']; ?>" id="lname" name="username" required>
							  <?php echo form_error('username', '<p class="error">', '</p>'); ?>	
							</div>
							
							<div class="form-group">
							  <label for="usr">Email:</label>
							  <input type="email" class="form-control" value="<?php echo $user[0]['email']; ?>" id="email" name="email" required>
							  <?php echo form_error('email', '<p class="error">', '</p>'); ?>
							</div>
							<div class="form-group">
								<div class="input-group image-preview">
								<input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
								<span class="input-group-btn">
                    <!-- image-preview-clear button -->
							<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
								<span class="glyphicon glyphicon-remove"></span> Clear
							</button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
		
				   </div>
                </span>
				
            </div>
			<span class="error"><?php if(!empty($error)){ echo $error['error'];}?></span>
			</div>
			<button type="submit" class="btn waves-effect waves-light">
                                     <span>Submit</span>
                                 </button>
			</form>
                </div>
            </div>
        </div>
    </div>


    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>
        </div>
    </div>

</div>



</body>
</html>