
	
	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/notifications/sweet_alert.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/pages/datatables_advanced.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/pages/components_modals.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/pages/dashboard.js"></script>
	

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->
<script type="text/javascript">
	

 $(".storedelete").click(function(e){
			 
			  var id=$(this).attr('id');
			  var btn = this;
		 	var table = $('#datatables-example').DataTable();
			swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Store!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
				if (willDelete) {
					
				$.ajax({
	            url: "<?php echo site_url()?>"+'Store/storedelete',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						
						swal("Poof! Your Store has been deleted!", {
						icon: "success",
						});
						table.row($(btn).parents('tr')).remove().draw(false);
					}
				}
					});
					
				} 
				
            else {
                swal({
                    title: "Cancelled",
                    text: "Your  file is safe :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
				});
			
		 
		  
		 });
	// Alert combination
    $('.storedeleted').on('click', function() {
    		  var id=$(this).attr('id');
			  var btn = this;
			  // alert(id);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Store!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {


$.ajax({
	            url: "<?php echo site_url()?>"+'Store/storedelete',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						swal({
                    title: "Deleted!",
                    text: "Your Store has been deleted.",
                    confirmButtonColor: "#66BB6A",
                    type: "success"
                });
						table.row($(btn).parents('tr')).remove().draw(false);
					}
				}
					});


                
            }
            else {
                swal({
                    title: "Cancelled",
                    text: "Your  file is safe :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    });
</script>
<script type="text/javascript">
  $(document).ready(function(){
	var success='<?php echo $this->session->userdata('success');?>';
		if(success==1)
		{
			swal("Store has been updated successfully", "You clicked the button!", "success");
			<?php $this->session->unset_userdata('success');?>
		}
  });
</script>
	 
	 <script type="text/javascript">
	//date
	$(document).ready(function(){
	$('.dateAnimate').bootstrapMaterialDatePicker({format : 'YYYY', weekStart : 0, time: false,animation:true});
    $('.date').bootstrapMaterialDatePicker({ weekStart : 0, time: false});
    $('.time').bootstrapMaterialDatePicker({ date: false,format:'HH:mm',animation:true});
    $('.datetime').bootstrapMaterialDatePicker({ format : 'dddd DD MMMM YYYY - HH:mm',animation:true});
    $('.date-fr').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', lang : 'fr', weekStart : 1, cancelText : 'ANNULER'});
    $('.min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', minDate : new Date() });

  });
</script>
	  <script type="text/javascript">
	  
 
    $(document).ready(function() {
		
	  $("#next2").click(function(e){
		  $("#tabs-demo6 li:nth-child(1)").addClass("active");
						$("#tabs-demo6 li:nth-child(2)").removeClass("active");
						$("#tabs-demo6-area2").removeClass("in active");
						$("#tabs-demo6-area1").addClass("in active");
	  });
	  $("#editnext1").click(function(e){
		 
		  $("#tabs-demo6 li:nth-child(2)").addClass("active");
						$("#tabs-demo6 li:nth-child(1)").removeClass("active");
						$("#tabs-demo6-area1").removeClass("in active");
						$("#tabs-demo6-area2").addClass("in active");
						
	    }); 
	  
	  $("#next1").click(function(e){
		    //alert();
	    	e.preventDefault();
	    	var name = $("input[name='name']").val();
	    	var categoryid = $("select[name='categoryid']").val();
			var subcategoryid = $("select[name='subcategoryid']").val();
	    	var email = $("input[name='email']").val();
	    	var mobile = $("input[name='mobile']").val();
	    	var description = $("input[name='description']").val();
	    	var address = $("input[name='address']").val();
			var phone1 = $("input[name='phone1']").val();
			var phone2 = $("input[name='phone2']").val();
			var phone3 = $("input[name='phone3']").val();
			var other = $("input[name='other']").val();
	    	var city = $("select[name='city']").val();
	    	var area1 = $("select[name='area']").val();
			var landline=phone1+''+phone2+''+phone3;
			
			
	        $.ajax({
	            url: "<?php echo site_url()?>"+'Validation/addStoreNext1',
	            type:'POST',
	            dataType: "json",
	            data: {landline:landline,name:name,subcategoryid:subcategoryid, categoryid:categoryid, email:email, mobile:mobile,description:description,city:city,area1:area1,address:address,other:other},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						$("#tabs-demo6 li:nth-child(2)").addClass("active");
						$("#tabs-demo6 li:nth-child(1)").removeClass("active");
						$("#tabs-demo6-area1").removeClass("in active");
						$("#tabs-demo6-area2").addClass("in active");
						
					}
						if($.isEmptyObject(data.name)){
							$("#name_err").hide();
						}else{
						$("#name_err").show();
	                	$("#name_err").html(data.name);
						}
						if($.isEmptyObject(data.categoryid)){
							
						$("#category_err").hide();
						}
						else{
						$("#category_err").show();
	                	$("#category_err").html(data.categoryid);
						}
						if($.isEmptyObject(data.email)){
						$("#email_err").hide();
						}
						else{
						$("#email_err").show();
	                	$("#email_err").html(data.email);
						}
						if($.isEmptyObject(data.mobile)){
						$("#mobile_err").hide();
						}
						else{
						$("#mobile_err").show();
	                	$("#mobile_err").html(data.mobile);
						}
						if($.isEmptyObject(data.area1)){
						$("#area1_err").hide();
						}
						else{
						$("#area1_err").show();
	                	$("#area1_err").html(data.area1);
						}
						if($.isEmptyObject(data.description)){
						$("#description_err").hide();
						}
						else{
						$("#description_err").show();
	                	$("#description_err").html(data.description);
						}
						if($.isEmptyObject(data.city)){
						$("#city_err").hide();
						}
						else{
						$("#city_err").show();
	                	$("#city_err").html(data.city);
						}
						if($.isEmptyObject(data.address)){
							
						$("#address_err").hide();
						}
						else{
						$("#address_err").show();
	                	$("#address_err").html(data.address);
						}
	            }
	        });
	    }); 
	});
	
  
</script>
	 <script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
	
  });
</script>
<script type="text/javascript">
//use in addstore
    $(document).ready(function() {

        $("#tag").tagit();
        
    });
   
    
//CHANGE STATE
 $(document).ready(function() {
        $("#categoryid").change(function(){
			
		var categoryid=$(this).find('option:selected').attr("id");
		//alert(categoryid);
		
		if (categoryid == '') {
		}else{
		$.ajax({
		url:"<?php echo site_url();?>getsubcategory",
		type:"POST",
		data:{'categoryid':categoryid},
		dataType:'json',
		success:function(data){
			//alert(data);
			$('.subcategoryid').html(data);
		}
		});
		}
        });
    });

 $(document).ready(function() {

        $(".states").change(function(){
           
     var state_id=$(this).find('option:selected').attr("id");
// alert(state_id);
if (state_id == '') {
}else{
    $.ajax({
url:"<?php echo site_url();?>"+'Store/getCity',
type:"POST",
data:{'state_id':state_id},
dataType:'json',
success:function(data){
    // alert(data);
    $('.cities').html(data);
}

    });

}
        });

          
    });
//image code
function preview_images() 
{
 var total_file=document.getElementById("images").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<div class='col-md-3' ><img class='img-responsive' style='height:100px; width:100px;    margin-bottom: 20px;' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
 }
}

$('#add_more').click(function() {
          "use strict";
          $(this).before($("<div/>", {
            id: 'filediv'
          }).fadeIn('slow').append(
            $("<input/>", {
              name: 'file[]',
              type: 'file',
              id: 'file',
              multiple: 'multiple',
              accept: 'image/*'
            })
          ));
        });

        $('#upload').click(function(e) {
          "use strict";
          e.preventDefault();

          if (window.filesToUpload.length === 0 || typeof window.filesToUpload === "undefined") {
            alert("No files are selected.");
            return false;
          }

          // Now, upload the files below...
          // https://developer.mozilla.org/en-US/docs/Using_files_from_web_applications#Handling_the_upload_process_for_a_file.2C_asynchronously
        });

        deletePreview = function (ele, i) {
          "use strict";
          try {
            $(ele).parent().remove();
            window.filesToUpload.splice(i, 1);
          } catch (e) {
            console.log(e.message);
          }
        }


	 $("#images").on('change', function() {
          "use strict";
// $('#image_preview').remove();
          // create an empty array for the files to reside.
          window.filesToUpload = [];

          if (this.files.length >= 1) {
            $('#previewimg').remove();
            $.each(this.files, function(i, img) {
              var reader = new FileReader(),
                newElement = $("<div id='previewimg" + i + "' class='previewBox' height='50px' width='50px'><img /></div>"),
                deleteBtn = $("<span class='delete' onClick='deletePreview(this, " + i + ")'>X</span>").prependTo(newElement),
                preview = newElement.find("img");

              reader.onloadend = function() {
                preview.attr("src", reader.result);
                preview.attr("alt", img.name);
              };

              try {
                window.filesToUpload.push(document.getElementById("file").files[i]);
              } catch (e) {
               // console.log(e.message);
              }

              if (img) {
                reader.readAsDataURL(img);
              } else {
                preview.src = "";
              }

              newElement.appendTo("#filediv");
            });
          }
        });


      (function(jQuery){

        // start: Chart =============

        Chart.defaults.global.pointHitDetectionRadius = 1;
        Chart.defaults.global.customTooltips = function(tooltip) {

            var tooltipEl = $('#chartjs-tooltip');

            if (!tooltip) {
                tooltipEl.css({
                    opacity: 0
                });
                return;
            }

            tooltipEl.removeClass('above below');
            tooltipEl.addClass(tooltip.yAlign);

            var innerHtml = '';
            if (undefined !== tooltip.labels && tooltip.labels.length) {
                for (var i = tooltip.labels.length - 1; i >= 0; i--) {
                    innerHtml += [
                        '<div class="chartjs-tooltip-section">',
                        '   <span class="chartjs-tooltip-key" style="background-color:' + tooltip.legendColors[i].fill + '"></span>',
                        '   <span class="chartjs-tooltip-value">' + tooltip.labels[i] + '</span>',
                        '</div>'
                    ].join('');
                }
                tooltipEl.html(innerHtml);
            }

            tooltipEl.css({
                opacity: 1,
                left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
                top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
                fontFamily: tooltip.fontFamily,
                fontSize: tooltip.fontSize,
                fontStyle: tooltip.fontStyle
            });
        };
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var lineChartData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: "My First dataset",
                fillColor: "rgba(21,186,103,0.4)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(66,69,67,0.3)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                 data: [18,9,5,7,4.5,4,5,4.5,6,5.6,7.5]
            }, {
                label: "My Second dataset",
                fillColor: "rgba(21,113,186,0.5)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [4,7,5,7,4.5,4,5,4.5,6,5.6,7.5]
            }]
        };

        var doughnutData = [
                {
                    value: 300,
                    color:"#129352",
                    highlight: "#15BA67",
                    label: "Alfa"
                },
                {
                    value: 50,
                    color: "#1AD576",
                    highlight: "#15BA67",
                    label: "Beta"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#15BA67",
                    label: "Gamma"
                },
                {
                    value: 40,
                    color: "#0F5E36",
                    highlight: "#15BA67",
                    label: "Peta"
                },
                {
                    value: 120,
                    color: "#15A65D",
                    highlight: "#15BA67",
                    label: "X"
                }

            ];


        var doughnutData2 = [
                {
                    value: 100,
                    color:"#129352",
                    highlight: "#15BA67",
                    label: "Alfa"
                },
                {
                    value: 250,
                    color: "#FF6656",
                    highlight: "#FF6656",
                    label: "Beta"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#15BA67",
                    label: "Gamma"
                },
                {
                    value: 40,
                    color: "#FD786A",
                    highlight: "#15BA67",
                    label: "Peta"
                },
                {
                    value: 120,
                    color: "#15A65D",
                    highlight: "#15BA67",
                    label: "X"
                }

            ];

        var barChartData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(21,186,103,0.4)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(21,186,103,0.2)",
                        highlightStroke: "rgba(21,186,103,0.2)",
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(21,113,186,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(21,113,186,0.2)",
                        highlightStroke: "rgba(21,113,186,0.2)",
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };

         window.onload = function(){
                var ctx = $(".doughnut-chart")[0].getContext("2d");
                window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
                    responsive : true,
                    showTooltips: true
                });

                var ctx2 = $(".line-chart")[0].getContext("2d");
                window.myLine = new Chart(ctx2).Line(lineChartData, {
                     responsive: true,
                        showTooltips: true,
                        multiTooltipTemplate: "<%= value %>",
                     maintainAspectRatio: false
                });

                var ctx3 = $(".bar-chart")[0].getContext("2d");
                window.myLine = new Chart(ctx3).Bar(barChartData, {
                     responsive: true,
                        showTooltips: true
                });

                var ctx4 = $(".doughnut-chart2")[0].getContext("2d");
                window.myDoughnut2 = new Chart(ctx4).Doughnut(doughnutData2, {
                    responsive : true,
                    showTooltips: true
                });

            };
        
        //  end:  Chart =============

        // start: Calendar =========
         $('.dashboard .calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '2015-02-12',
            businessHours: true, // display business hours
            editable: true,
            events: [
                {
                    title: 'Business Lunch',
                    start: '2015-02-03T13:00:00',
                    constraint: 'businessHours'
                },
                {
                    title: 'Meeting',
                    start: '2015-02-13T11:00:00',
                    constraint: 'availableForMeeting', // defined below
                    color: '#20C572'
                },
                {
                    title: 'Conference',
                    start: '2015-02-18',
                    end: '2015-02-20'
                },
                {
                    title: 'Party',
                    start: '2015-02-29T20:00:00'
                },

                // areas where "Meeting" must be dropped
                {
                    id: 'availableForMeeting',
                    start: '2015-02-11T10:00:00',
                    end: '2015-02-11T16:00:00',
                    rendering: 'background'
                },
                {
                    id: 'availableForMeeting',
                    start: '2015-02-13T10:00:00',
                    end: '2015-02-13T16:00:00',
                    rendering: 'background'
                },

                // red areas where no events can be dropped
                {
                    start: '2015-02-24',
                    end: '2015-02-28',
                    overlap: false,
                    rendering: 'background',
                    color: '#FF6656'
                },
                {
                    start: '2015-02-06',
                    end: '2015-02-08',
                    overlap: true,
                    rendering: 'background',
                    color: '#FF6656'
                }
            ]
        });
        // end : Calendar==========

        // start: Maps============

          jQuery('.maps').vectorMap({
            map: 'world_en',
            backgroundColor: null,
            color: '#fff',
            hoverOpacity: 0.7,
            selectedColor: '#666666',
            enableZoom: true,
            showTooltip: true,
            values: sample_data,
            scaleColors: ['#C8EEFF', '#006491'],
            normalizeFunction: 'polynomial'
        });

        // end: Maps==============

      })(jQuery);
     </script>
	 <script>
	$(document).ready(function(){
		$("#subcategory").click(function(){
			alert();
			var subcategory=$("#as").val();
			var id=$("#id").val();
			var dataString = 'subcategory=' + subcategory +'&id=' + id;
			$.ajax
                ({
                    type: "POST",
                    url: "<?php echo site_url()?>"+'addSubCategory/addnew',
                    data: dataString,
                    cache: false,
                    success: function (data)
                    {
                       $(location).prop('href', 'categories')
					   //window.location.href = "categories";
                    }
                });
		});
	});
	
	</script>
<!-- Footer -->
					<div class="footer text-muted">
						            <p>© 2018 All rights reserved <a href="<?php echo base_url(); ?>"> Quickfinder</a></p>

					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>