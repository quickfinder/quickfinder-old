<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-indigo">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<div class="navbar-right">
<ul class="nav navbar-nav">	

<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
								<?php echo $this->session->userdata('email');?>									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="<?php echo site_url('Profile');?>"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
                     <li><a href="<?php echo site_url('changePassword')?>"><i class="icon-cog5"></i> <span>Change Password</span></a></li>
								
								
								<li><a href="<?php echo site_url('logout');?>"><i class="icon-switch2"></i> <span>Logout</span></a></li>
								</ul>
							</li>			
								
				</ul>

				<!-- <p class="navbar-text"><?php echo $this->session->userdata('email');?></p> -->
				<p class="navbar-text"><span class="label bg-success-400">Online</span></p>
				
				<ul class="nav navbar-nav">				
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-bell2"></i>
							<span class="visible-xs-inline-block position-right">Activity</span>
							<span class="status-mark border-orange-400"></span>
						</a>

						<div class="dropdown-menu dropdown-content">
							<div class="dropdown-content-heading">
								Activity
								<ul class="icons-list">
									<li><a href="#"><i class="icon-menu7"></i></a></li>
								</ul>
							</div>

						</div>
					</li>

<!-- 					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-bubble8"></i>
							<span class="visible-xs-inline-block position-right">Messages</span>
							<span class="status-mark border-orange-400"></span>
						</a>
						
						
					</li>	 -->				
				</ul>
			</div>
		</div>
	</div>
	<!-- /main navbar -->

	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-default">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user-material">
						<div class="category-content">
							<div class="sidebar-user-material-content">
								<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-responsive" alt=""></a>
								<h6><?php echo $this->session->userdata('email');?></h6>
								<!-- <span class="text-size-small">Santa Ana, CA</span> -->
							</div>
														
							<div class="sidebar-user-material-menu">
								<a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
							</div>
						</div>
						
						<div class="navigation-wrapper collapse" id="user-nav">
							<ul class="navigation">
							<li><a href="<?php echo site_url('Profile');?>"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
                     <li><a href="<?php echo site_url('changePassword')?>"><i class="icon-cog5"></i> <span>Change Password</span></a></li>
								
								
								<li><a href="<?php echo site_url('logout');?>"><i class="icon-switch2"></i> <span>Logout</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /user menu -->
<?php

$usertype = $this->session->userdata('usertype');
if ($usertype == 4) {
    ?>

					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="<?php echo site_url('dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Manage Users</span></a>
									<ul>
										 <li><a href="<?php echo site_url('Users'); ?>">Admin</a></li>
                            <li><a href="<?php echo site_url('vendorslist'); ?>">Vendors</a></li>
                            <li><a href="<?php echo site_url('userlist'); ?>">Users</a></li>
									</ul>
								</li>
								<li>
								<a  href="<?php echo site_url('categories'); ?>"><i class="icon-copy"></i> <span> Manage Categories</span></a>




                        </li>
                        <li>
                        <a  href="<?php echo site_url('storelist'); ?>"><i class="icon-film"></i> <span> Stores</span></a>
                        	 
                        </li>
                        <li>
                          <a  href="<?php echo site_url('banner-list'); ?>"><i class="icon-film"></i> <span> Manage Banner</span></a>
                        	
                        </li>
									
								
								
								
								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->
 <?php
    } elseif ($usertype == 3) {
        ?>
<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="<?php echo site_url('dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								
								<li>
								<a  href="<?php echo site_url('storelist'); ?>"><i class="icon-copy"></i> <span> Stores</span></a>
                        </li>
                        <li>
                        <a  href="<?php echo site_url('adminstore'); ?>"><i class="icon-film"></i> <span>Your Stores</span></a>
                        	 
                        </li>
								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

<?php
        }
    elseif($usertype==2)
	{
		?>


				<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="<?php echo site_url('dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								
								<li>
								<a  href="<?php echo site_url('');?>"><i class=" icon-home5"></i> <span> Home</span></a>
                        </li>
                       <?php if($this->session->userdata('num_store')>=1 ){?>
					<li class="active ripple">
                      <a href="<?php echo site_url('storelist');?>"><span class="icons icon-film"></span> Manage Stores 
                      </a>
                    </li>
					<?php }?>
					
                       	
								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

					
		  <?php
	}
	elseif($usertype==1)
	{
		?>


						<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="<?php echo site_url('dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								
								<li>
								<a  href="#"><i class="icon-copy"></i> <span> feedback & Reviews </span></a>
                        </li>
								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->
                  
		<?php
	}
	
?>
				</div>
			</div>
			<!-- /main sidebar -->
