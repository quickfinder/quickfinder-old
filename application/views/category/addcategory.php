<?php ?>
<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Category</h3>
                <p class="animated fadeInDown">
                    Form <span class="fa-angle-right fa"></span> Add New Category
                </p>
            </div>
        </div>
    </div>
    <div class="form-element">
        <div class="col-md-12">
            <div class="col-md-12 panel">
                <div class="col-md-12 panel-heading">
                    <h4>Add New Category Form</h4>
                </div>
                <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                    <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" enctype="multipart/form-data" action="<?php echo site_url('addNewCategory'); ?>">
                            <div class="col-md-6">
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" id="validate_firstname" name="name" required>
                                    <span class="bar"></span>
                                    <label>Category Name</label>
                                </div>
                                <div class="form-group">
                                    <div class="input-group image-preview">
                                        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                <span class="glyphicon glyphicon-remove"></span> Clear
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="glyphicon glyphicon-folder-open"></span>
                                                <span class="image-preview-input-title">Browse</span>
                                                <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->

                                            </div>

                                        </span>
                                    </div>
                                    <span class="error"><?php
                                        if (!empty($error)) {
                                            echo $error['error'];
                                        }
                                        ?></span>
                                </div>

                            </div>                   
                            <div class="col-md-12">
                                <input class="submit btn btn-danger" type="submit" value="Submit">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>