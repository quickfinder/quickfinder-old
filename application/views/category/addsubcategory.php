		<div id="content">
                <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Sub Category</h3>
                        <p class="animated fadeInDown">
                          Form <span class="fa-angle-right fa"></span> Add Sub Category
                        </p>
                    </div>
                  </div>
                </div>
                <div class="form-element">
				<div class="col-md-12">
                  <div class="col-md-12 panel">
                    <div class="col-md-12 panel-heading">
                      <h4>Add Sub Category Form</h4>
                    </div>
                    <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                      <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" action="<?php echo site_url('addSubCategory/addnew')?>">
                          <div class="col-md-6">
							<?php 
							
								$name='';
								$categoryname='';
								$categoriesid='';
								if(!empty($subcategory))
								{
									foreach($subcategory as $row)
									{
										$name.=$row['subcategoriesname'].',';
										$categoryname=$row['categoriesname'];
										$categoriesid=$row['categoriesid'];
									}
								}
								else
								{
									print_r($subcategory);
								}
									
							?>                           
						   <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" id="validate_firstname" value="<?php echo $categoryname;?>" name="name" required>
                              <input type="hidden" class="form-text" id="id" value="<?php echo $categoriesid;?>" name="id" required>
                              <span class="bar"></span>
                              <label>Category Name</label>
                            </div>
						
							<div class="bs-example" >
		
							<textarea name="subcategory" id="demo4"><?php echo $name;?></textarea>
							</div>
                            
                          </div>                   
                          <div class="col-md-12">
                              
                             <!-- <input class="submit btn btn-danger"  type="submit" value="Submit" style="margin-top:20px !important;">-->
							  <a href="<?php echo site_url('backtosubcategories');?>"> <input   style="margin-top:20px !important;"class="submit btn btn-success" type="button" value="Back"></a>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
              </div>
			  </div>
			  </div>