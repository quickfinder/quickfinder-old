<!-- start: Content -->
 <style>
 .tags input, li.addedTag {
    border: 1px solid transparent;
    border-radius: 2px;
    box-shadow: none;
    display: block;
    padding: 0.5em;
}
@-moz-document url-prefix() {
  .tags {
    display: inline;
    padding:0em;
    width: 100%;
	}
	ul{
	list-style-type: disc;
    margin-before: 1em;
    margin-after: 1em;
    margin-start: 0px;
    margin-end: 0px;
    padding-start: 40px;
	}
}
@media screen and (-webkit-min-device-pixel-ratio:0) {
.tags{
	display:inline;
    padding:30em;
    width:100%;
}
}
	
.tags li.tagAdd, .tags li.addedTag {
    float: left;
    margin-left: 0.25em;
    margin-right: 0.25em;
}
.tags li.addedTag {
    background: none repeat scroll 0 0 #019f86;
    border-radius: 2px;
    color: #fff;
    padding: 0.25em;
	margin-bottom: 2px;
}
</style>
  
<div id="content">

<!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Category List</span></h4>
                        </div>

                        
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Category</a></li>
                            <li class="active">Category List</li>
                        </ul>

                        
                    </div>
                </div>
                <!-- /page header -->


   
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3>
                        <a href="<?php echo site_url('addCategory'); ?>">
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Category</span>
                            </button>
                            
                        </a>	

                         	  </h3></div>
                <div class="panel-body">

                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr . No</th>
                                    <th>Category Name</th>
									<th class="td">Icon</th>
                                    <th>Subcategories</th>
                                    <th>Operations </th>
                                </tr>
                            </thead>
                            <tbody>
                                
								<?php
								
                                if (is_array($categorylist)) {
                                    $i = 0;
                                    foreach ($categorylist as $row) {
										
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $row['category']['name']; ?></td>
                                            <td><img class="icon-img" width="50px" hight="50px" src="<?php echo base_url()?><?php echo $row['category']['icons']?>"></td>
											
											<td class="td"><?php
											
											$sub_cats = Array();
											echo "<ul class='tags'>";
											foreach($row['subcategories'] as $u) 
											{
												echo "<li class='addedTag'>".$u['name']."</li>";
											}
											echo "</ul>";
											//$str = implode(",",$sub_cats);
											//echo $str;
											?>
											
											</td>
											
                                            <td>  
																		
                            <a href="<?php echo site_url('addSubCategory/'.$row['category']['id']); ?>" class="btn btn-gradient btn-info" data-toggle="tooltip" data-placement="top" title="Edit Subcategories"><i class="fa fa-edit"></i></a>
							<?php if($row['category']['IsActive']==1){?>
							<a href="<?php echo site_url('deactiveCategory/'.$row['category']['id']); ?>" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Active Store">Active</i></a>
							<?php }else{ ?>
							<a href="<?php echo site_url('activeCategory/'.$row['category']['id']); ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Inactive Store">Inactive</a>
							<?php 	
							} ?>
							<a href="#" id="<?php echo $row['category']['id'];?>" class="deletecategories">
									<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Category" >
										<span class="fa fa-bitbucket"></span>
									</button>
									</a>
									<a href="<?php echo site_url('editcategories/'.$row['category']['id']); ?>" id="<?php echo $row['category']['id'];?>" >
									<button class="btn btn-gradient btn-warning" data-toggle="tooltip" data-placement="top" title="Edit Category" >
										<span class="fa fa-pencil"></span>
									</button>
									</a>
							
                        </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td>NO Record Found !...</td>

                                    </tr>
                                    <?php
                                }
                                ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<!-- end: content -->
