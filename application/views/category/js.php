
<script type="text/javascript">
$(document).ready(function(){
	$("#datatables-example").on("click", ".deletecategories", function(){
	 
		  var id=$(this).attr('id');
		  var btn = this;
		  var table = $('#datatables-example').DataTable();
		  swal({
					  title: "Are you sure?",
					  text: "Once deleted, you will not be able to recover !",
					  icon: "warning",
					  buttons: true,
					  dangerMode: true,
				})
				.then((willDelete) => {
				if (willDelete) 
				{
				$.ajax({
	            url: "<?php echo site_url()?>"+'Category/categorydelete',
	            type:'POST',
	            dataType: "json",
	            data: {id:id},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						swal("Poof! Your Record has been deleted!", {
						icon: "success",
						});
						table.row($(btn).parents('tr')).remove().draw(false);
					}
				}
					});	
				} 
				else {
					swal("Your data is safe!");
					}
				});
	  });
	  $('#demo4').tagEditor({
    initialTags: [],
    placeholder: 'Enter tags ...',
	
    onChange: function(field, editor, tags) 
	{
        $('#response').prepend(
            'Tags changed to: ' + (tags.length ? tags.join(', ') : '----') + '<hr>'
        );
		

    },
	
    beforeTagSave: function(field, editor, tags, tag, val) {
        $('#response').prepend('Tag ' + val + ' saved' + (tag ? ' over ' + tag : '') + '.');
		var id=$('#id').val();
		
		$.ajax({
	            url: "<?php echo site_url()?>"+'Category/changeSubcategory',
	            type:'POST',
	            dataType: "json",
	            data: {id:id,tag:tag,val:val},
	            success: function(data) 
				{
					if(data.sucess==1)
					{
						swal("Poof! Your subcategory has been Updated!", {
						icon: "success",
						});
					}
					else{
						
					}
				}
			});
			$.ajax({
	            url: "<?php echo site_url()?>"+'Category/addNewSubcategory/',
	            type:'POST',
	            dataType: "json",
	            data: {id:id,val:val},
	            success: function(data) 
				{
					
					if(data.sucess==1)
					{
						swal("Poof! Your subcategory Inserted Successfully!", {
						icon: "success",
						});
					}
				}
			});
	
	},
    beforeTagDelete: function(field, editor, tags, val) 
	{
		var id=$('#id').val();
		 var q = confirm('Remove tag "' + val + '"?');
		 if(q)
		 {
	   
				$('#response').prepend('Tag ' + val + ' deleted.');
				$.ajax({
	            url: "<?php echo site_url()?>"+'Category/deleteSubcategory',
	            type:'POST',
	            dataType: "json",
	            data: {id:id,val:val},
	            success: function(data) 
				{
					if(data.sucess==1)
					{	
						swal("Good job!", "Your subcategory has been Deleted!", "success");						
					}
				}
				});	
				
		 }
		 else{
			 $('#response').prepend('Removal of ' + val + ' discarded.');
		 }
				return q   
    }
});
	 
});
</script>